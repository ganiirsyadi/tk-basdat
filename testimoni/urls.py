from django.urls import path

from . import views

app_name = "testimoni"

urlpatterns = [
    path('', views.read_testimoni, name='read_testimoni'),
    path('update/', views.update_testimoni, name='update_testimoni'),
    path('create/', views.create_testimoni, name='create_testimoni'),
    path('delete/', views.delete_testimoni, name='delete_testimoni'),
]
