import datetime
from django.db import connection
from django.shortcuts import redirect, render

# Create your views here.


def read_testimoni(request):
    if request.session["user_role"] in ["staff", "pelanggan", "kurir"]:
        data = []
        context = {}
        # mengambil semua testimoni
        with connection.cursor() as c:
            c.execute("select * from testimoni")
            dataSQL = c.fetchall()
            # convert date object to str (date object ada di index ke-1 dan ke-2)
            for x in dataSQL:
                data.append((x[0], str(x[1]), str(x[2]), x[3], x[4]))
            context["data"] = data
        return render(request, 'testimoni/daftar_testimoni.html', context)


def update_testimoni(request):
    if request.session["user_role"] == "pelanggan":
        context = {}
        if request.method == "GET":
            # jika data ini ada di parameter get
            try:
                context["email"] = request.GET["email"]
                context["tanggalTransaksi"] = request.GET["tanggalTransaksi"]
                context["tanggalTestimoni"] = request.GET["tanggalTestimoni"]
                context["status"] = request.GET["status"]
                context["rating"] = request.GET["rating"]
            # jika tidak ada
            except:
                return redirect('testimoni:read_testimoni')
            return render(request, 'testimoni/update_testimoni.html', context)
        if request.method == 'POST':
            # melakukan update
            with connection.cursor() as c:
                email = request.POST["email"]
                tanggalTransaksi = request.POST["tanggalTransaksi"]
                tanggalTestimoni = datetime.datetime.now()
                status = request.POST["status"]
                rating = request.POST["rating"]
                command = f"update testimoni set rating = {rating}, "
                command += f"tanggal_testimoni = '{tanggalTestimoni}', status = '{status}' "
                command += f"where email = '{email}' and tanggal_transaksi = '{tanggalTransaksi}'"
                # jika update berhasil
                try:
                    c.execute(command)
                    print("success")
                # jika gagal
                except Exception as e:
                    print(e)
            return redirect('testimoni:read_testimoni')

def delete_testimoni(request):
    if request.session["user_role"] == "pelanggan":
        if request.method == "POST":
            with connection.cursor() as c:
                email = request.POST["email"]
                tanggalTransaksi = request.POST["tanggalTransaksi"]
                tanggalTestimoni = request.POST["tanggalTestimoni"]
                command = f"delete from testimoni where email = '{email}' and "
                command += f"tanggal_transaksi = '{tanggalTransaksi}' and tanggal_testimoni = '{tanggalTestimoni}' "
                try:
                    c.execute(command)
                    print("success")
                except Exception as e:
                    print(e)
                return redirect('testimoni:read_testimoni')

def create_testimoni(request):
    if request.session["user_role"] == "pelanggan":
        context = {}
        data = []
        if request.method == 'POST':
            try:
                email = request.POST["email"]
                tanggalTransaksi = request.POST["tanggalTransaksi"]
                tanggalTestimoni = datetime.datetime.now()
                status = request.POST["status"]
                rating = request.POST["rating"]

                # insert data
                with connection.cursor() as c:
                    command = "INSERT INTO testimoni VALUES "
                    command += str((str(email), str(tanggalTransaksi), str(tanggalTestimoni),
                                    str(status), rating))
                    c.execute(command)
            except Exception as e:
                print(e)
                context["error"] = "Data yang Anda masukkan tidak valid mohon cek kembali"
                #Ambil query lagi untuk di pass pada dropdown
                with connection.cursor() as c:
                    c.execute("select email, tanggal from transaksi_laundry")
                    dataSQL = c.fetchall()
                    for x in dataSQL:
                        data.append((x[0], str(x[1])))
                    context["data"] = data
                return render(request, 'testimoni/create_testimoni.html', context)
            return redirect('testimoni:read_testimoni')
        else:
            # mengambil semua email
            with connection.cursor() as c:
                c.execute("select email, tanggal from transaksi_laundry")
                dataSQL = c.fetchall()
                for x in dataSQL:
                    data.append((x[0], str(x[1])))
                context["data"] = data
            return render(request, 'testimoni/create_testimoni.html', context)