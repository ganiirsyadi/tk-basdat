from django.urls import path

from . import views

app_name = "item"

urlpatterns = [
    path('', views.read_item, name='read_item'),
    path('create/', views.create_item, name='create_item'),
]
