from django.db import connection
from django.db.utils import IntegrityError
from django.shortcuts import redirect, render

# Create your views here.

def read_item(request):
    if request.session["user_role"] in ["staff", "pelanggan", "kurir"]:
        context = {}
        with connection.cursor() as c:
            c.execute("select * from item")
            context["daftar_item"] = c.fetchall()
        return render(request, 'item/read_item.html', context)

def create_item(request):
    if request.session["user_role"] == "staff":
        context = {}
        if request.method == 'POST':
            with connection.cursor() as c:
                try:
                    kode_item = request.POST["kode_item"]
                    nama_item = request.POST["nama_item"]
                    c.execute(f"insert into item values ('{kode_item}', '{nama_item}')")
                    return redirect('item:read_item')
                except IntegrityError as e:
                    context["error"] = "Kode item tersebut sudah ada, mohon gunakan kode lainnya"
                    return render(request, 'item/create_item.html', context)
                except Exception as e:
                    context["error"] = "Data yang Anda masukkan tidak valid mohon cek kembali"
                    return render(request, 'item/create_item.html', context)
        return render(request, 'item/create_item.html')
