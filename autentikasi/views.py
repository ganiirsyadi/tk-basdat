from django.db.utils import IntegrityError
from django.shortcuts import redirect, render
from django.db import  connection

# Create your views here.

def home(request):
    return render(request, 'autentikasi/home.html')

def login_view(request):
    context = {}
    if request.method == "POST":
        role = ""
        email = request.POST["email"]
        password = request.POST["password"]
        with connection.cursor() as c:
            c.execute(f"select * from pengguna where email = '{email}' and password = '{password}'")
            row = c.fetchone()
            if row != None:
                c.execute(f"select * from pelanggan where email = '{email}'")
                pelanggan = c.fetchone()
                c.execute(f"select * from staf where email = '{email}'")
                staf = c.fetchone()
                c.execute(f"select * from kurir where email = '{email}'")
                kurir = c.fetchone()
                if pelanggan:
                    role = "pelanggan"
                if staf:
                    role = "staff"
                if kurir:
                    role = "kurir"
                
                request.session["user_email"] = email
                request.session["user_role"] = role
            else:
                context["error"] = "email atau password salah"
                return render(request, 'autentikasi/login.html', context)
        return redirect("autentikasi:home")
    return render(request, 'autentikasi/login.html')

def register_view(request):
    context = {}
    if request.method == "POST":
        email = request.POST["email"]
        password = request.POST["password"]
        name = request.POST["name"]
        hp = request.POST["hp"]
        gender = request.POST["gender"]
        address = request.POST["address"]
        if request.POST["role"] == "pelanggan":
            date = request.POST["date"]
            no_va = ""
            with  connection.cursor() as c:
                # mengambil nomor virtual account terbesar terakhir
                c.execute("select max(no_virtual_account) from pelanggan")
                row = c.fetchone()
                # jika table pelanggan sudah ada isinya
                if row[0] != None:
                    no_va = str(int(row[0]) + 1)
                # jika table pelanggan masih kosong
                else:
                    no_va = "100000001"
                # insert pelanggan baru
                try:
                    pengguna = (email, password, name, address, hp, gender)
                    pelanggan = (email, no_va, 0, date)
                    c.execute(f"insert into pengguna values {str(pengguna)}", )
                    c.execute(f"insert into pelanggan values {str(pelanggan)}")
                    request.session["user_email"] = email
                    request.session["user_role"] = request.POST["role"]
                    return redirect("autentikasi:home")
                # jika sudah ada email terdaftar maka failed
                except IntegrityError:
                    context["error"] = "email sudah terdaftar"
                    return render(request, 'autentikasi/register.html', context)
        if request.POST["role"] == "staff" or request.POST["role"] == "kurir":
            npwp = request.POST["npwp"]
            norek = request.POST["norek"]
            bank = request.POST["bank"]
            cabang = request.POST["cabang"]
            # jika staf
            if request.POST["role"] == "staff":
                with connection.cursor() as c:
                    # insert staf baru
                    try:
                        pengguna = (email, password, name, address, hp, gender)
                        staf = (email, npwp, norek, bank, cabang)
                        c.execute(f"insert into pengguna values {str(pengguna)}", )
                        c.execute(f"insert into staf values {str(staf)}")
                        request.session["user_email"] = email
                        request.session["user_role"] = request.POST["role"]
                        return redirect("autentikasi:home")
                    # jika sudah ada email terdaftar maka failed
                    except IntegrityError:
                        context["error"] = "email sudah terdaftar"
                        return render(request, 'autentikasi/register.html', context)
            elif request.POST["role"] == "kurir":
                sim = request.POST["sim"]
                nokendaraan = request.POST["nokendaraan"]
                jeniskendaraan = request.POST["jeniskendaraan"]
                with connection.cursor() as c:
                    # insert kurir baru
                    try:
                        pengguna = (email, password, name, address, hp, gender)
                        kurir = (email, npwp, norek, bank, cabang, sim, nokendaraan, jeniskendaraan)
                        c.execute(f"insert into pengguna values {str(pengguna)}", )
                        c.execute(f"insert into kurir values {str(kurir)}")
                        request.session["user_email"] = email
                        request.session["user_role"] = request.POST["role"]
                        return redirect("autentikasi:home")
                    # jika sudah ada email terdaftar maka failed
                    except IntegrityError:
                        context["error"] = "email sudah terdaftar"
                        return render(request, 'autentikasi/register.html', context)
    return render(request, 'autentikasi/register.html')

def logout_view(request):
    request.session["user_email"] = ""
    request.session["user_role"] = ""
    return redirect('autentikasi:home')