$("#btn-role").click(e => {
  e.preventDefault()
  const role = $('select[name="role"]').val();
  if (role != $("#btn-back").data("active-role")) {
    $("#choose-role").toggleClass("d-none");
    $("#bottom-button").toggleClass("d-none")
    $("#bottom-button").toggleClass("d-flex")
    switch (role) {
      case "staff":
        $("#role-form").html(staff);
        $("#btn-back").data("active-role", "staff");
        break;
      case "kurir":
        $("#role-form").html(kurir);
        $("#btn-back").data("active-role", "kurir");
        break;
      case "pelanggan":
        $("#role-form").html(pelanggan);
        $("#btn-back").data("active-role", "pelanggan");
        break;
      default:
        break;
    }
  }
});
$("#btn-back").click((e) => {
  e.preventDefault();
  $("#choose-role").toggleClass("d-none");
  $("#role-form").html("");
  $("#bottom-button").toggleClass("d-none")
  $("#bottom-button").toggleClass("d-flex")
});

const staff = `
<h5 class="text-center mb-3">Masukkan data diri Anda</h5>
<div class="form-group">
  <label for="email">Email</label>
  <input
    type="email"
    name="email"
    class="form-control"
    id="email"
    placeholder="Email"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="password">Password</label>
  <input
    type="password"
    name="password"
    class="form-control"
    id="password"
    placeholder="Password"
    maxlength="50"
    required
  />
</div>  
<div class="form-group">
  <label for="name">Nama Lengkap</label>
  <input
    type="text"
    name="name"
    class="form-control"
    id="name"
    placeholder="Nama lengkap"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="hp">Nomor Telepon</label>
  <input
    type="text"
    name="hp"
    class="form-control"
    id="hp"
    placeholder="Nomor Telepon"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="gender">Jenis Kelamin</label>
  <select name="gender" id="gender" class="form-control" required>
    <option selected disabled>Pilih...</option>
    <option value="M">Pria</option>
    <option value="F">Wanita</option>
  </select>
</div>
<div class="form-group">
  <label for="address">Alamat</label>
  <textarea class="form-control" name="address" id="address" cols="30" rows="5"></textarea>
</div>
<div class="form-group">
  <label for="npwp">NPWP</label>
  <input
    type="number"
    name="npwp"
    class="form-control"
    id="npwp"
    placeholder="Nomor Pokok Wajib Pajak"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="norek">Nomor Rekening</label>
  <input
    type="number"
    name="norek"
    class="form-control"
    id="norek"
    placeholder="Nomor Rekening"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="bank">Nama Bank</label>
  <input
    type="text"
    name="bank"
    class="form-control"
    id="bank"
    placeholder="Nama Bank"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="cabang">Kantor Cabang</label>
  <input
    type="text"
    name="cabang"
    class="form-control"
    id="cabang"
    placeholder="Kantor Cabang"
    maxlength="50"
    required
  />
</div>
`

const kurir = `
<h5 class="text-center mb-3">Masukkan data diri Anda</h5>
<div class="form-group">
  <label for="email">Email</label>
  <input
    type="email"
    name="email"
    class="form-control"
    id="email"
    placeholder="Email"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="password">Password</label>
  <input
    type="password"
    name="password"
    class="form-control"
    id="password"
    placeholder="Password"
    maxlength="50"
    required
  />
</div>  
<div class="form-group">
  <label for="name">Nama Lengkap</label>
  <input
    type="text"
    name="name"
    class="form-control"
    id="name"
    placeholder="Nama lengkap"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="hp">Nomor Telepon</label>
  <input
    type="text"
    name="hp"
    class="form-control"
    id="hp"
    placeholder="Nomor Telepon"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="gender">Jenis Kelamin</label>
  <select name="gender" id="gender" class="form-control" required>
    <option selected disabled>Pilih...</option>
    <option value="M">Pria</option>
    <option value="F">Wanita</option>
  </select>
</div>
<div class="form-group">
  <label for="address">Alamat</label>
  <textarea class="form-control" name="address" id="address" cols="30" rows="5"></textarea>
</div>
<div class="form-group">
  <label for="npwp">NPWP</label>
  <input
    type="number"
    name="npwp"
    class="form-control"
    id="npwp"
    placeholder="Nomor Pokok Wajib Pajak"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="norek">Nomor Rekening</label>
  <input
    type="number"
    name="norek"
    class="form-control"
    id="norek"
    placeholder="Nomor Rekening"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="bank">Nama Bank</label>
  <input
    type="text"
    name="bank"
    class="form-control"
    id="bank"
    placeholder="Nama Bank"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="cabang">Kantor Cabang</label>
  <input
    type="text"
    name="cabang"
    class="form-control"
    id="cabang"
    placeholder="Kantor Cabang"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="sim">Nomor SIM</label>
  <input
    type="text"
    name="sim"
    class="form-control"
    id="sim"
    placeholder="Nomor SIM"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="nokendaraan">Nomor Kendaraan</label>
  <input
    type="text"
    name="nokendaraan"
    class="form-control"
    id="nokendaraan"
    placeholder="Nomor Kendaraan"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="jeniskendaraan">Jenis Kendaraan</label>
  <input
    type="text"
    name="jeniskendaraan"
    class="form-control"
    id="jeniskendaraan"
    placeholder="Jenis Kendaraan"
    maxlength="20"
    required
  />
</div>
`

const pelanggan = `
<h5 class="text-center mb-3">Masukkan data diri Anda</h5>
<div class="form-group">
  <label for="email">Email</label>
  <input
    type="email"
    name="email"
    class="form-control"
    id="email"
    placeholder="Email"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="password">Password</label>
  <input
    type="password"
    name="password"
    class="form-control"
    id="password"
    placeholder="Password"
    maxlength="50"
    required
  />
</div>  
<div class="form-group">
  <label for="name">Nama Lengkap</label>
  <input
    type="text"
    name="name"
    class="form-control"
    id="name"
    placeholder="Nama lengkap"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="hp">Nomor Telepon</label>
  <input
    type="text"
    name="hp"
    class="form-control"
    id="hp"
    placeholder="Nomor Telepon"
    maxlength="20"
    required
  />
</div>
<div class="form-group">
  <label for="gender">Jenis Kelamin</label>
  <select name="gender" id="gender" class="form-control" required>
    <option selected disabled>Pilih...</option>
    <option value="M">Pria</option>
    <option value="F">Wanita</option>
  </select>
</div>
<div class="form-group">
  <label for="date">Tanggal Lahir</label>
  <input
    type="date"
    name="date"
    class="form-control"
    id="date"
    placeholder="Tanggal Lahir"
    required
  />
</div>
<div class="form-group">
  <label for="address">Alamat</label>
  <textarea class="form-control" name="address" id="address" cols="30" rows="5"></textarea>
</div>
`