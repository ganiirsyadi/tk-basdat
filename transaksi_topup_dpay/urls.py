from django.urls import path
from . import views

app_name = "transaksi_topup_dpay"

urlpatterns = [
    path('', views.daftar_transaksi_topup_dpay, name='daftar_transaksi_topup_dpay'),
    path('create/', views.create_transaksi_topup_dpay, name='create_transaksi_topup_dpay'),
    path('update/', views.update_transaksi_topup_dpay, name='update_transaksi_topup_dpay'),
    path('update/<str:email>/<str:tanggal>', views.update_transaksi_topup_dpay, name='update_transaksi_topup_dpay'),
    path('delete/<str:email>/<str:tanggal>', views.delete_transaksi_topup_dpay, name='delete_transaksi_topup_dpay'),
]
