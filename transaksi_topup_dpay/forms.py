from django import forms
from datetime import datetime

class CreateTransaksiTopupDpayForm(forms.Form):
    def __init__(self, email_choices=None, *args, **kwargs):
        super(CreateTransaksiTopupDpayForm, self).__init__(*args, **kwargs)
        self.fields['email'].choices = email_choices

    email = forms.ChoiceField(label='Email:', required=True, choices=())
    tanggal = forms.DateTimeField(label='Tanggal:', widget=forms.HiddenInput(), initial=datetime.now(), required=True)
    nominal = forms.DecimalField(label='Nominal:', required=True)

class UpdateTransaksiTopupDpayForm(forms.Form):
    email = forms.EmailField(label='Email:', disabled=True, required=True)
    tanggal = forms.DateTimeField(label='Tanggal:', disabled=True, required=True)
    nominal = forms.DecimalField(label='Nominal:', required=True)
