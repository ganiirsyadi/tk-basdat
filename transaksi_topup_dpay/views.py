from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreateTransaksiTopupDpayForm, UpdateTransaksiTopupDpayForm

"""
# This is mockup code
def daftar_transaksi_topup_dpay(request):
    return render(request, 'transaksi_topup_dpay/daftar_transaksi_topup_dpay.html')

def create_transaksi_topup_dpay(request):
    if request.method == 'POST':
        return redirect('transaksi_topup_dpay:daftar_transaksi_topup_dpay')
    return render(request, 'transaksi_topup_dpay/create_transaksi_topup_dpay.html')

def update_transaksi_topup_dpay(request):
    if request.method == 'POST':
        return redirect('transaksi_topup_dpay:daftar_transaksi_topup_dpay')
    return render(request, 'transaksi_topup_dpay/update_transaksi_topup_dpay.html')

# Uncomment only after database schema is in place
"""

def daftar_transaksi_topup_dpay(request):
    with connection.cursor() as cursor:
        cursor.execute("select * from transaksi_topup_dpay")
        data = []
        for x in cursor.fetchall():
            data.append((str(x[0]), str(x[1]), x[2]))
        context = {"daftar_transaksi_topup_dpay" : data}
    return render(request, "transaksi_topup_dpay/daftar_transaksi_topup_dpay.html", context)

def create_transaksi_topup_dpay(request):
    with connection.cursor() as cursor:
        cursor.execute("select email from transaksi_topup_dpay")
        choice =  [(_[0], _[0]) for _ in cursor.fetchall()]
        cursor.execute("select email, tanggal, nominal from transaksi_topup_dpay")

        if request.method == "POST":
            form = CreateTransaksiTopupDpayForm(choice, request.POST)
            if form.is_valid():
                cursor.execute("insert into transaksi_topup_dpay (email, tanggal, nominal) values (%s, %s, %s)",
                    [
                        form.cleaned_data['email'],
                        form.cleaned_data['tanggal'],
                        form.cleaned_data['nominal']
                    ]
                )
                return redirect('transaksi_topup_dpay:daftar_transaksi_topup_dpay')
        else:
            form = CreateTransaksiTopupDpayForm(choice)
        return render(request, 'transaksi_topup_dpay/create_transaksi_topup_dpay.html', {'form' : form})

def update_transaksi_topup_dpay(request, email, tanggal):
    with connection.cursor() as cursor:
        cursor.execute(f"select email, tanggal, nominal from transaksi_topup_dpay where email = '{email}' and tanggal = '{tanggal}'")
        data = cursor.fetchall()[0]
        data_initial = {
            'email' : data[0],
            'tanggal' : data[1],
            'nominal' : data[2]
        }

        if request.method == "POST":
            form = UpdateTransaksiTopupDpayForm(request.POST, initial=data_initial)
            if form.is_valid():
                cursor.execute(f"update transaksi_topup_dpay set nominal = {nominal} where email = '{email}' and tanggal = '{tanggal}'")
                return redirect('transaksi_topup_dpay:daftar_transaksi_topup_dpay')
        else:
            form = UpdateTransaksiTopupDpayForm(initial=data_initial)
        return render(request, 'transaksi_topup_dpay/update_transaksi_topup_dpay.html', {'form' : form})

def delete_transaksi_topup_dpay(request, email, tanggal):
    with connection.cursor() as cursor:
        cursor.execute(f"delete from transaksi_topup_dpay where email = '{email}' and tanggal = '{tanggal}'")
        return render(request, 'transaksi_topup_dpay/daftar_transaksi_topup_dpay.html')
