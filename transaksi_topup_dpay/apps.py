from django.apps import AppConfig


class TransaksiTopupDpayConfig(AppConfig):
    name = 'transaksi_topup_dpay'
