from django.apps import AppConfig


class StatusTransaksiConfig(AppConfig):
    name = 'status_transaksi'
