from django.urls import path

from . import views

app_name = "status_transaksi"

urlpatterns = [
    path('', views.read_status_transaksi, name='read_status_transaksi'),
    path('update/', views.update_status_transaksi, name='update_status_transaksi'),
    path('delete/', views.delete_status_transaksi, name='delete_status_transaksi'),
]
