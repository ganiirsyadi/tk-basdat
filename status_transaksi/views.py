from django.shortcuts import redirect, render
from django.db import connection

# Create your views here.

def read_status_transaksi(request):
    if request.session["user_role"] in ["staff", "pelanggan", "kurir"]:
        context = {}
        data = []
        with connection.cursor() as c:
            c.execute("select * from status_transaksi")
            dataSQL = c.fetchall()
            for x in dataSQL:
                data.append((x[0], str(x[1]), x[2], str(x[3])))
            context["daftar_status"] = data
        return render(request, 'status_transaksi/daftar_status_transaksi.html', context)

def update_status_transaksi(request):
    if request.session["user_role"] == "staff":
        context = {}
        if request.method == "GET":
            # jika data ini ada di parameter get
            try:
                context["email"] = request.GET["email"]
                context["tanggal"] = request.GET["tanggal"]
                context["kode"] = request.GET["kode"]
                context["timestamp"] = request.GET["timestamp"]
            # jika tidak ada
            except:
                return redirect('status_transaksi:read_status_transaksi')
            # mengambil semua kode item
            with connection.cursor() as c:
                c.execute("select * from status")
                context["daftar_status"] = c.fetchall()
            return render(request, 'status_transaksi/update_status_transaksi.html', context)
        if request.method == 'POST':
            # melakukan update
            with connection.cursor() as c:
                email = request.POST["email"]
                tanggal = request.POST["tanggal"]
                kode = request.POST["kode_status"]
                old_kode = request.POST["old_kode_status"]
                timestamp = request.POST["timestamp"]
                command = f"update status_transaksi set kode_status = '{kode}', "
                command += f"time_stamp = '{timestamp}' where email = '{email}'"
                command += f"and tanggal_transaksi = '{tanggal}' and kode_status = '{old_kode}'"
                # jika update berhasil
                try:
                    print(command)
                    c.execute(command)
                    print("success")
                # jika gagal
                except Exception as e:
                    print(e)
            return redirect('status_transaksi:read_status_transaksi')

def delete_status_transaksi(request):
    if request.session["user_role"] == "staff":
        if request.method == "POST":
            with connection.cursor() as c:
                email = request.POST["email"]
                tanggal = request.POST["tanggal"]
                kode = request.POST["kode"]
                command = f"delete from status_transaksi where email = '{email}' and "
                command += f"tanggal_transaksi = '{tanggal}' and kode_status = '{kode}'"
                try:
                    c.execute(command)
                    print("success")
                except Exception as e:
                    print(e)
                return redirect('status_transaksi:read_status_transaksi')
