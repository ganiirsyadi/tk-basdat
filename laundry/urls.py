from django.urls import path

from . import views

app_name = "laundry"

urlpatterns = [
    path('', views.read_laundry, name='read_laundry'),
    path('update/', views.update_laundry, name='update_laundry'),
    path('create/', views.create_laundry, name='create_laundry'),
    path('delete/', views.delete_laundry, name='delete_laundry')
]
