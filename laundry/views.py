import datetime
from django.db import connection
from django.shortcuts import redirect, render

# Create your views here.


def read_laundry(request):
    if request.session["user_role"] in ["staff", "pelanggan"]:
        data = []
        context = {}
        # mengambil semua daftar laundry
        with connection.cursor() as c:
            if request.session["user_role"] == "pelanggan":
                email = request.session["user_email"]
                c.execute(f"select * from daftar_laundry where email = '{email}'")
            else:
                c.execute("select * from daftar_laundry")
            dataSQL = c.fetchall()

            # convert date object to str (date object ada di index ke-1)
            for x in dataSQL:
                data.append((x[0], str(x[1]), x[2], x[3], x[4]))
            context["data"] = data
        return render(request, 'laundry/daftar_laundry.html', context)


def update_laundry(request):
    if request.session["user_role"] == "staff":
        context = {}
        if request.method == "GET":
            # jika data ini ada di parameter get
            try:
                context["email"] = request.GET["email"]
                context["tanggal"] = request.GET["tanggal"]
                context["no_urut"] = request.GET["no_urut"]
                context["jumlah"] = request.GET["jumlah"]
                context["kode"] = request.GET["kode"]
            # jika tidak ada
            except:
                return redirect('laundry:read_laundry')
            # mengambil semua kode item
            with connection.cursor() as c:
                c.execute("select * from item")
                context["daftar_kode"] = c.fetchall()
            return render(request, 'laundry/update_laundry.html', context)
        if request.method == 'POST':
            # melakukan update
            with connection.cursor() as c:
                email = request.POST["email"]
                tanggal = request.POST["tanggal"]
                no_urut = request.POST["no_urut"]
                jumlah_item = request.POST["jumlah_item"]
                kode_item = request.POST["kode_item"]
                command = f"update daftar_laundry set "
                command += f"jumlah_item = {jumlah_item}, kode_item = '{kode_item}' "
                command += f"where email = '{email}' and tanggal_transaksi = '{tanggal}' "
                command += f"and no_urut = {no_urut}"
                # jika update berhasil
                try:
                    c.execute(command)
                    print("success")
                # jika gagal
                except Exception as e:
                    print(e)
            return redirect('laundry:read_laundry')


def delete_laundry(request):
    if request.session["user_role"] == "staff":
        if request.method == "POST":
            with connection.cursor() as c:
                email = request.POST["email"]
                tanggal = request.POST["tanggal"]
                no_urut = request.POST["no_urut"]
                command = f"delete from daftar_laundry where email = '{email}' and "
                command += f"tanggal_transaksi = '{tanggal}' and no_urut = {no_urut}"
                try:
                    c.execute(command)
                    print("success")
                except Exception as e:
                    print(e)
                return redirect('laundry:read_laundry')


def create_laundry(request):
    if request.session["user_role"] == "staff":
        context = {}
        if request.method == 'POST':
            try:
                email = request.POST["email"]
                tanggal = request.POST["tanggal"]
                jumlah_item = request.POST["jumlah_item"]
                kode_item = request.POST["kode_item"]
                # mencari max no_urut
                no_urut = 1
                with connection.cursor() as c:
                    # mengambil nomor urut terbesar terakhir
                    c.execute(
                        f"select max(no_urut) from daftar_laundry where email = '{email}'")
                    row = c.fetchone()
                    # jika table pelanggan sudah ada isinya
                    if row[0] != None:
                        no_urut = str(int(row[0]) + 1)

                # insert data
                with connection.cursor() as c:
                    command = "INSERT INTO daftar_laundry VALUES "
                    command += str((str(email), str(tanggal), no_urut,
                                    jumlah_item, str(kode_item)))
                    c.execute(command)
            except Exception as e:
                print(e)
                context["error"] = "Data yang Anda masukkan tidak valid mohon cek kembali"
                # mengambil semua kode
                with connection.cursor() as c:
                    c.execute("select * from item")
                    context["daftar_kode"] = c.fetchall()
                # mengambil semua email
                with connection.cursor() as c:
                    c.execute("select email from pelanggan")
                    context["daftar_email"] = c.fetchall()
                return render(request, 'laundry/create_laundry.html', context)
            return redirect('laundry:read_laundry')
        else:
            data = []
            # mengambil semua kode
            with connection.cursor() as c:
                c.execute("select * from item")
                context["daftar_kode"] = c.fetchall()
            # mengambil semua email
            with connection.cursor() as c:
                c.execute("select email, tanggal from transaksi_laundry")
                dataSQL = c.fetchall()
                for x in dataSQL:
                    data.append((x[0], str(x[1])))
                context["data"] = data
            return render(request, 'laundry/create_laundry.html', context)
