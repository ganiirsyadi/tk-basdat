--
-- Database: SILAU
--

-- --------------------------------------------------------
CREATE SCHEMA SILAU;
SET SEARCH_PATH TO SILAU;

--
-- Table structure for table pengguna
--

create table pengguna (
  email varchar(50) primary key,
  password varchar(50) not null,
  nama_lengkap varchar(50) not null,
  alamat text,
  no_hp varchar(20) not null,
  jenis_kelamin char(1) not null 
  constraint pilihan check 
  (jenis_kelamin = 'F' or jenis_kelamin = 'M')
);

--
-- Dumping data for table pengguna
--

INSERT INTO "pengguna" VALUES
    ('kbarsham0@epa.gov','YC32j3U2JXvn','Kristoforo Barsham','07 Utah Court',3029369873,'F'),
    ('osante1@noaa.gov','48gAsUD','Odette Sante','75421 Johnson Drive',7002063051,'F'),
    ('malessandrelli2@thetimes.co.uk','LDPQSc','Michaeline Alessandrelli','3 Daystar Place',2871403634,'M'),
    ('cgravy3@marketwatch.com','rT4QDus5','Cyrill Gravy','8 Dryden Terrace',1785622581,'F'),
    ('wyersin4@bizjournals.com','6ly7at','Wiatt Yersin','1 Granby Point',4961252083,'M'),
    ('nclimpson5@hostgator.com','1oq282g','Nat Climpson','102 Mallory Lane',3332293732,'M'),
    ('dharston6@usgs.gov','EZbilVv','Doro Harston','78 Southridge Way',1685394494,'M'),
    ('edewey7@feedburner.com','09apzt','Enrichetta Dewey','28 Florence Point',5717175208,'F'),
    ('jcarslaw8@hhs.gov','oiCZSf','Jo Carslaw','26360 Meadow Ridge Crossing',1847099970,'M'),
    ('fcarolan9@npr.org','cB97mJm','Ferdinanda Carolan',NULL,5599230735,'F'),
    ('strencha@sourceforge.net','ve6FtChoG3ej','Sharai Trench','6 Waxwing Way',1602680785,'M'),
    ('dcrewb@aboutads.info','wQ9uEw5DyTu','Doro Crew',NULL,7818754442,'F'),
    ('mdawdaryc@nytimes.com','gsR7kDys','Marin Dawdary','233 Vernon Road',7764554617,'F'),
    ('tpittemd@state.tx.us','vWm8zT5zY','Tamara Pittem',NULL,4658231070,'F'),
    ('efantee@dmoz.org','BwO78Xrp','Elayne Fante','688 Fordem Center',9153777035,'F'),
    ('nkrishtopaittisf@cnbc.com','mP1UTlcVNsyh','Nicola Krishtopaittis','6 American Road',8551787546,'F'),
    ('sgyppsg@google.fr','prKhh5p08','Stesha Gypps','54971 Bowman Park',8078967693,'F'),
    ('amaghullh@cbc.ca','jGh5eX0','Amanda Maghull','49257 Kropf Circle',7641929108,'M'),
    ('ssarli@paypal.com','zgOTtx','Stephanie Sarl','758 Lillian Place',8143368253,'M'),
    ('chousamanj@studiopress.com','Iab44fhuc2M','Casi Housaman','574 Kingsford Way',9957000253,'F'),
    ('ahatliffek@exblog.jp','aqqQpEV','Althea Hatliffe','0 Emmet Plaza',3986895983,'F'),
    ('vwallingl@nsw.gov.au','WHMTFrYBLX','Verena Walling','445 Cordelia Circle',5311162396,'F'),
    ('mzorzonim@friendfeed.com','MiK6KiL2Yu','Minnnie Zorzoni','95887 Cardinal Terrace',9937775371,'F'),
    ('colssonn@yolasite.com','0Jyjle','Curr Olsson','24 Buell Circle',4144432098,'F'),
    ('gmcgeechano@aboutads.info','cjgWD9Xacd','Gerti McGeechan',NULL,9194000279,'F'),
    ('wwillsonp@yale.edu','TTJu9j6rK4','Witty Willson','3 Nancy Drive',6801964714,'F'),
    ('tpackerq@edublogs.org','7oZlZUgW','Ty Packer',NULL,6227017712,'F'),
    ('lhuddler@sbwire.com','YZprzXmKIKz','Laurel Huddle','4072 Anhalt Road',7675494248,'M'),
    ('vkirtlands@indiegogo.com','b95go7NXoAON','Vicky Kirtland','2499 Colorado Hill',5616829786,'F'),
    ('qclassent@newsvine.com','kveUFORS0GVc','Quintilla Classen',NULL,5975710343,'F'),
    ('pcordovau@ning.com','IhaJ0ZyiEdt','Phillie Cordova','9 Dayton Alley',6201499833,'M'),
    ('wwherrettv@google.com','M9WInzXwXjv','Waldo Wherrett','3 Dennis Court',4704687224,'F'),
    ('tboydlew@ning.com','gcwTBDuC8','Teddie Boydle',NULL,4592723734,'M'),
    ('wgobyx@uol.com.br','WBJKXM2z','Wilmar Goby',NULL,5115474436,'F'),
    ('dpattissony@moonfruit.com','JoGTStVf4','Daune Pattisson','80 Clemons Plaza',1108842354,'F'),
    ('thutablez@symantec.com','bc1Lf7Vi3X','Tadeo Hutable','5 Pine View Parkway',3946135418,'M'),
    ('pcarneck10@goo.ne.jp','BKrUOo8593im','Prisca Carneck',NULL,2553336457,'M'),
    ('dtoppas11@booking.com','WdF5T9ein1K','Donnie Toppas','0 Golf Course Crossing',4834534584,'M'),
    ('ieloi12@ox.ac.uk','LBWnZ0CWcGQ','Ivory Eloi','5442 Hazelcrest Point',3459967924,'F'),
    ('htitford13@xrea.com','oBF7W9jvqb6','Holly-anne Titford',NULL,3603138780,'M'),
    ('jallner14@liveinternet.ru','F4JhrsTecH','Julieta Allner','5706 Eastlawn Center',2887038041,'F'),
    ('dpeiro15@si.edu','2PF9fr','Dallon Peiro','608 Rutledge Center',3269356013,'F'),
    ('cvost16@paypal.com','Z8c8hJI1J','Callean Vost','624 Chinook Park',8824048186,'M'),
    ('tleemans17@squarespace.com','J4fmIzk','Tomas Leemans','133 Armistice Plaza',1514167039,'M'),
    ('gtours18@godaddy.com','8FW5jr','Gene Tours','84 Rockefeller Terrace',9306906756,'M'),
    ('gporter19@jiathis.com','4Ry4WyiX','Giustino Porter','6 Northwestern Parkway',3749712407,'F'),
    ('gclampton1a@instagram.com','ywokb1SH','Gallagher Clampton','9 Schurz Trail',6485135266,'M'),
    ('nedwinson1b@about.com','Rj0OibEY','Noni Edwinson','08931 Esch Junction',5966558053,'M'),
    ('kcharke1c@imgur.com','UbUmozBv','Kay Charke','33017 Red Cloud Point',5469588071,'M'),
    ('ewinkworth1d@cnet.com','I1Y4b5VuiV','Elvina Winkworth','80 Arapahoe Hill',7045346922,'F'),
    ('ltabb1e@imageshack.us','bvIsXoDip','Lefty Tabb','029 Blackbird Alley',5481541988,'F'),
    ('hbeebe1f@about.com','V8Rugh','Hatty Beebe','0570 Prentice Point',1218403586,'F'),
    ('bfurmage1g@reddit.com','5T4989rqy','Braden Furmage','42088 Randy Center',3445992461,'M'),
    ('dfellini1h@globo.com','jBy33E','Daron Fellini','154 Corry Point',1946234061,'M'),
    ('jchilvers1i@cafepress.com','pdwRt31YOS','Julie Chilvers','70880 Loeprich Junction',9312548651,'M'),
    ('mbrahmer1j@ezinearticles.com','kNoq8yn','Morten Brahmer',NULL,6723459970,'F'),
    ('rhugli1k@google.co.jp','qhxcrpQ5MFEu','Rae Hugli','3 Little Fleur Circle',5312550402,'M'),
    ('rillwell1l@ibm.com','NkHaegnqjM7','Raf Illwell','13443 Continental Trail',8038365881,'M'),
    ('agamble1m@yelp.com','kcwia9kk','Anestassia Gamble','81441 7th Point',1037542321,'F'),
    ('dbert1n@java.com','30xt2NU','Dar Bert',NULL,1669459886,'F'),
    ('golijve1o@mashable.com','TGFUFGIoi','Giuditta Olijve',NULL,3523227485,'M'),
    ('tlovell1p@tinypic.com','npZjkpFng','Tonya Lovell',NULL,8607938183,'F'),
    ('bwithinshaw1q@upenn.edu','a73y2A','Brier Withinshaw','83 Bonner Avenue',2375113537,'F'),
    ('cmcgloin1r@vistaprint.com','TmCaveM4QPn','Colver McGloin','2615 Kensington Park',6247982176,'F'),
    ('bbaitey1s@npr.org','Nme87o9W','Bruno Baitey','3 Fairfield Alley',3925376139,'M'),
    ('scotes1t@hud.gov','9HQAmSY9HOLs','Sondra Cotes','9028 Maywood Junction',5387807284,'M'),
    ('iwoodyatt1u@google.com.hk','eFokWKoPG','Inger Woodyatt','5893 Becker Way',4698217724,'M'),
    ('kkneeshaw1v@ehow.com','89ayKlCM','Kippy Kneeshaw','960 Rutledge Trail',1833772474,'M'),
    ('hviollet1w@independent.co.uk','0AipCvp','Hanny Viollet','80 Burrows Plaza',4025325665,'M'),
    ('clothlorien1x@unc.edu','9xlcOVGu','Christoph Lothlorien',NULL,5887317982,'F'),
    ('jharkins1y@wp.com','1vkPhWZUKGll','Janifer Harkins',NULL,7143586539,'M'),
    ('pspaven1z@usatoday.com','zsMcsZ2','Prescott Spaven','13 Blaine Trail',5989291646,'F'),
    ('acandwell20@noaa.gov','rKSpizqgb','Austine Candwell','2703 Farragut Way',5092352635,'F'),
    ('apresslie21@pen.io','2iSAkuA1gP','Aili Presslie','8 Riverside Trail',4287679280,'M'),
    ('iotridge22@bloglines.com','TnnVOc1','Ileana Otridge',NULL,1578985893,'M'),
    ('tmarchello23@newyorker.com','WO69tG','Terrijo Marchello','198 Spohn Avenue',7425507723,'M'),
    ('ahuie24@bbc.co.uk','DxxbNklC67','Abigail Huie',NULL,8534948370,'F'),
    ('tdottrell25@marketwatch.com','pVHmO7Zn4OpZ','Trace Dottrell','451 Sommers Trail',6174197941,'M'),
    ('fdowry26@who.int','aXUOmT9oI','Felic Dowry','3203 Merry Junction',4872138160,'M'),
    ('ppedrick27@nsw.gov.au','nsBdFdE1qj','Prentiss Pedrick','7839 Mendota Plaza',2002354226,'M'),
    ('bbrixham28@ycombinator.com','ZTp9FpuL','Brandon Brixham','770 Myrtle Parkway',9189006822,'M'),
    ('kgobel29@technorati.com','ErNTeUGNwXcp','Kit Gobel','78481 Ilene Place',5014462426,'M'),
    ('lwitherspoon2a@cbsnews.com','SfIN08','Lauritz Witherspoon',NULL,6829366544,'M'),
    ('gturbat2b@msu.edu','Zaahua','Gallagher Turbat',NULL,6466018361,'F'),
    ('cgladyer2c@dion.ne.jp','2t5g93Chp','Candida Gladyer','6 Butterfield Court',1826814690,'M'),
    ('hgelderd2d@hud.gov','7bP3aoK0H7','Hamlin Gelderd','9899 Atwood Alley',1067015139,'F'),
    ('lcollcott2e@multiply.com','5f8fy8UsIwjo','Lorne Collcott','7 Hollow Ridge Lane',9015639976,'F'),
    ('llamprey2f@youtu.be','eGyjFeMFv','Leo Lamprey','072 Sheridan Park',6765080046,'M'),
    ('ctranfield2g@creativecommons.org','dU1ZW4LXk','Carolyne Tranfield','0 Rusk Point',7751651809,'M'),
    ('ngomersall2h@seesaa.net','4HJyyav','Netti Gomersall','4173 Carey Crossing',2054531083,'M'),
    ('lpellman2i@jimdo.com','XFfzzC','Lion Pellman',NULL,4928026045,'F'),
    ('bbartosik2j@scribd.com','EQNDgt','Bendite Bartosik','15712 Straubel Road',3278742560,'F'),
    ('jtolworth2k@independent.co.uk','JMT1GwDVH','Johnath Tolworth','94 Emmet Way',5885101301,'M'),
    ('pdax2l@reference.com','CWZevBVInPfd','Pansie Dax',NULL,8626943749,'M'),
    ('gdubble2m@patch.com','bSOW7hNPCi','Gusella Dubble','74017 Onsgard Lane',9572986755,'M'),
    ('amewitt2n@slideshare.net','z875dn','Astra Mewitt','691 Hazelcrest Plaza',2258020052,'F'),
    ('dguerriero2o@boston.com','YLDzZf','Dayna Guerriero',NULL,9768448597,'F'),
    ('msnailham2p@ihg.com','huvzWwlc','Maible Snailham',NULL,1491350304,'M'),
    ('atuff2q@nymag.com','gkliJKVyEt','Alix Tuff','3562 Sommers Crossing',7056241962,'M'),
    ('psheara2r@discovery.com','29nNNuC9Z','Peg Sheara','5 Old Shore Crossing',8172215959,'M');

-- --------------------------------------------------------

--
-- Table structure for table item
--

create table item (
	kode_item varchar(10) primary key,
  	nama_item varchar(50) not null
);

--
-- Dumping data for table item
--

INSERT INTO "item" VALUES
    (1,'Baju'),
    (2,'Celana'),
    (3,'Jaket'),
    (4,'Sarung'),
    (5,'Kemeja'),
    (6,'Kaos'),
    (7,'Seragam'),
    (8,'Kaos Kaki'),
    (9,'Handuk'),
    (10,'Dasi');

-- --------------------------------------------------------

--
-- Table structure for table layanan
--

create table layanan (
	kode_layanan varchar(10) primary key,
  	nama_layanan varchar(50) not null,
  	durasi_layanan varchar(10) not null,
  	harga_layanan real not null
);

--
-- Dumping data for table layanan
--

INSERT INTO "layanan" VALUES
    ('R','Reguler','2 Hari',10000),
    ('E','Express','1 Hari',20000),
    ('SE','Super Express','3 Jam',30000);

-- --------------------------------------------------------

--
-- Table structure for table status
--

create table status (
	kode_status varchar(10) primary key,
  nama_status varchar(15) not null
);

--
-- Dumping data for table status
--

INSERT INTO "status" VALUES
    (1,'Dipesan'),
    (2,'Diproses'),
    (3,'Siap diambil'),
    (4,'Diantar'),
    (5,'Selesai');

-- --------------------------------------------------------

--
-- Table structure for table PELANGGAN
--

CREATE TABLE PELANGGAN(
	email varchar(50) NOT NULL,
	no_virtual_account varchar(20) NOT NULL,
	saldo_dpay real NOT NULL DEFAULT 0,
    tanggal_lahir date,
	PRIMARY KEY(email),
	FOREIGN KEY(email) REFERENCES PENGGUNA(email)
);

--
-- Dumping data for table PELANGGAN
--

INSERT INTO "pelanggan" VALUES
    ('kbarsham0@epa.gov',100000001,10000),
    ('osante1@noaa.gov',100000002,20000),
    ('malessandrelli2@thetimes.co.uk',100000003,50000),
    ('cgravy3@marketwatch.com',100000004,100000),
    ('wyersin4@bizjournals.com',100000005,10000),
    ('nclimpson5@hostgator.com',100000006,20000),
    ('dharston6@usgs.gov',100000007,50000),
    ('edewey7@feedburner.com',100000008,100000),
    ('jcarslaw8@hhs.gov',100000009,10000),
    ('fcarolan9@npr.org',100000010,20000),
    ('strencha@sourceforge.net',100000011,50000),
    ('dcrewb@aboutads.info',100000012,100000),
    ('mdawdaryc@nytimes.com',100000013,10000),
    ('tpittemd@state.tx.us',100000014,20000),
    ('efantee@dmoz.org',100000015,50000),
    ('nkrishtopaittisf@cnbc.com',100000016,100000),
    ('sgyppsg@google.fr',100000017,10000),
    ('amaghullh@cbc.ca',100000018,20000),
    ('ssarli@paypal.com',100000019,50000),
    ('chousamanj@studiopress.com',100000020,100000),
    ('ahatliffek@exblog.jp',100000021,10000),
    ('vwallingl@nsw.gov.au',100000022,20000),
    ('mzorzonim@friendfeed.com',100000023,50000),
    ('colssonn@yolasite.com',100000024,100000),
    ('gmcgeechano@aboutads.info',100000025,10000),
    ('wwillsonp@yale.edu',100000026,20000),
    ('tpackerq@edublogs.org',100000027,50000),
    ('lhuddler@sbwire.com',100000028,100000),
    ('vkirtlands@indiegogo.com',100000029,10000),
    ('qclassent@newsvine.com',100000030,20000),
    ('pcordovau@ning.com',100000031,50000),
    ('wwherrettv@google.com',100000032,100000),
    ('tboydlew@ning.com',100000033,10000),
    ('wgobyx@uol.com.br',100000034,20000),
    ('dpattissony@moonfruit.com',100000035,50000),
    ('thutablez@symantec.com',100000036,100000),
    ('pcarneck10@goo.ne.jp',100000037,10000),
    ('dtoppas11@booking.com',100000038,20000),
    ('ieloi12@ox.ac.uk',100000039,50000),
    ('htitford13@xrea.com',100000040,100000),
    ('jallner14@liveinternet.ru',100000041,10000),
    ('dpeiro15@si.edu',100000042,20000),
    ('cvost16@paypal.com',100000043,50000),
    ('tleemans17@squarespace.com',100000044,100000),
    ('gtours18@godaddy.com',100000045,10000),
    ('gporter19@jiathis.com',100000046,20000),
    ('gclampton1a@instagram.com',100000047,50000),
    ('nedwinson1b@about.com',100000048,100000),
    ('kcharke1c@imgur.com',100000049,10000),
    ('ewinkworth1d@cnet.com',100000050,20000),
    ('ltabb1e@imageshack.us',100000051,50000),
    ('hbeebe1f@about.com',100000052,100000),
    ('bfurmage1g@reddit.com',100000053,10000),
    ('dfellini1h@globo.com',100000054,20000),
    ('jchilvers1i@cafepress.com',100000055,50000),
    ('mbrahmer1j@ezinearticles.com',100000056,100000),
    ('rhugli1k@google.co.jp',100000057,10000),
    ('rillwell1l@ibm.com',100000058,20000),
    ('agamble1m@yelp.com',100000059,50000),
    ('dbert1n@java.com',100000060,100000),
    ('golijve1o@mashable.com',100000061,10000),
    ('tlovell1p@tinypic.com',100000062,20000),
    ('bwithinshaw1q@upenn.edu',100000063,50000),
    ('cmcgloin1r@vistaprint.com',100000064,100000),
    ('bbaitey1s@npr.org',100000065,10000),
    ('scotes1t@hud.gov',100000066,20000),
    ('iwoodyatt1u@google.com.hk',100000067,50000),
    ('kkneeshaw1v@ehow.com',100000068,100000),
    ('hviollet1w@independent.co.uk',100000069,10000),
    ('clothlorien1x@unc.edu',100000070,20000);

-- --------------------------------------------------------

--
-- Table structure for table TARIF_ANTAR_JEMPUT
--

CREATE TABLE TARIF_ANTAR_JEMPUT(
	kode varchar(10) NOT NULL,
	jarak_min int NOT NULL,
	jarak_max int NOT NULL,
	harga real NOT NULL,
	PRIMARY KEY(kode)
);

--
-- Dumping data for table TARIF_ANTAR_JEMPUT
--

INSERT INTO "tarif_antar_jemput" VALUES
    ('SR',0,1,6000),
    ('MR',1,2,9000),
    ('LR',2,4,15000);

-- --------------------------------------------------------

--
-- Table structure for table TRANSAKSI_TOPUP_DPAY
--

CREATE TABLE TRANSAKSI_TOPUP_DPAY(
	email varchar(50) NOT NULL,
	tanggal timestamp NOT NULL,
	nominal real NOT NULL,
	PRIMARY KEY(email, tanggal),
	FOREIGN KEY(email) REFERENCES PELANGGAN(email)
);

--
-- Dumping data for table TRANSAKSI_TOPUP_DPAY
--

INSERT INTO "transaksi_topup_dpay" VALUES
    ('kbarsham0@epa.gov','2020-05-30 09:04:57',10000),
    ('osante1@noaa.gov','2019-05-17 04:22:41',20000),
    ('malessandrelli2@thetimes.co.uk','2019-10-16 19:48:49',10000),
    ('cgravy3@marketwatch.com','2020-02-12 17:50:57',20000),
    ('wyersin4@bizjournals.com','2020-01-24 09:24:02',10000),
    ('nclimpson5@hostgator.com','2019-06-21 05:49:50',20000),
    ('dharston6@usgs.gov','2019-11-02 22:06:11',10000),
    ('edewey7@feedburner.com','2020-09-23 19:36:10',10000),
    ('jcarslaw8@hhs.gov','2020-01-17 12:44:25',20000),
    ('fcarolan9@npr.org','2020-12-04 13:49:35',10000),
    ('strencha@sourceforge.net','2020-04-05 13:10:19',20000),
    ('dcrewb@aboutads.info','2019-08-18 13:56:56',10000),
    ('mdawdaryc@nytimes.com','2019-11-28 17:37:35',20000),
    ('tpittemd@state.tx.us','2019-05-22 02:17:31',10000),
    ('efantee@dmoz.org','2019-08-29 22:47:11',10000),
    ('nkrishtopaittisf@cnbc.com','2020-10-09 16:11:50',20000),
    ('sgyppsg@google.fr','2020-03-20 10:44:06',10000),
    ('amaghullh@cbc.ca','2020-03-15 22:26:28',20000),
    ('ssarli@paypal.com','2020-09-23 04:10:46',10000),
    ('chousamanj@studiopress.com','2020-01-17 10:12:56',20000),
    ('ahatliffek@exblog.jp','2020-05-22 10:16:25',10000),
    ('vwallingl@nsw.gov.au','2019-04-30 09:22:50',10000),
    ('mzorzonim@friendfeed.com','2020-09-17 07:10:57',20000),
    ('colssonn@yolasite.com','2020-07-30 17:39:01',10000),
    ('gmcgeechano@aboutads.info','2020-03-30 21:40:16',20000),
    ('wwillsonp@yale.edu','2019-04-11 20:27:23',10000),
    ('tpackerq@edublogs.org','2020-09-29 22:46:54',20000),
    ('lhuddler@sbwire.com','2020-12-19 11:15:08',10000),
    ('vkirtlands@indiegogo.com','2020-03-04 05:29:18',10000),
    ('qclassent@newsvine.com','2019-10-08 02:04:35',20000),
    ('pcordovau@ning.com','2019-11-20 22:40:08',10000),
    ('wwherrettv@google.com','2020-04-01 17:30:27',20000),
    ('tboydlew@ning.com','2020-10-13 17:16:57',10000),
    ('wgobyx@uol.com.br','2019-04-11 05:02:13',20000),
    ('dpattissony@moonfruit.com','2019-05-26 08:42:35',10000),
    ('thutablez@symantec.com','2019-07-25 20:52:50',10000),
    ('pcarneck10@goo.ne.jp','2020-11-30 08:04:48',20000),
    ('dtoppas11@booking.com','2019-08-17 23:27:10',10000),
    ('ieloi12@ox.ac.uk','2019-09-20 14:30:04',20000),
    ('htitford13@xrea.com','2020-09-04 23:45:02',10000),
    ('jallner14@liveinternet.ru','2020-02-20 14:33:24',20000),
    ('dpeiro15@si.edu','2020-07-10 18:44:22',10000),
    ('cvost16@paypal.com','2020-08-31 16:57:46',10000),
    ('tleemans17@squarespace.com','2019-10-23 17:59:06',20000),
    ('gtours18@godaddy.com','2019-09-15 10:59:35',10000),
    ('gporter19@jiathis.com','2020-12-14 17:55:35',20000),
    ('gclampton1a@instagram.com','2019-04-09 11:27:58',10000),
    ('nedwinson1b@about.com','2020-10-31 19:54:59',20000),
    ('kcharke1c@imgur.com','2019-08-14 19:06:22',10000),
    ('ewinkworth1d@cnet.com','2020-04-06 12:21:05',10000),
    ('ltabb1e@imageshack.us','2019-08-03 10:38:49',20000),
    ('hbeebe1f@about.com','2019-08-13 05:16:51',10000),
    ('bfurmage1g@reddit.com','2019-10-08 16:23:26',20000),
    ('dfellini1h@globo.com','2020-05-05 02:59:32',10000),
    ('jchilvers1i@cafepress.com','2019-12-24 05:04:40',20000),
    ('mbrahmer1j@ezinearticles.com','2020-07-18 02:26:26',10000),
    ('rhugli1k@google.co.jp','2020-12-13 17:48:54',10000),
    ('rillwell1l@ibm.com','2019-05-12 14:38:18',20000),
    ('agamble1m@yelp.com','2019-05-16 06:07:39',10000),
    ('dbert1n@java.com','2020-07-16 00:26:26',20000);

-- --------------------------------------------------------

--
-- Table structure for table TRANSAKSI_LAUNDRY
--

CREATE TABLE TRANSAKSI_LAUNDRY(
	email varchar(50) NOT NULL,
	tanggal timestamp NOT NULL,
	berat integer NOT NULL,
	total_item integer NOT NULL,
	biaya_laundry real NOT NULL,
	biaya_antar real NOT NULL DEFAULT 0,
	biaya_jemput real NOT NULL DEFAULT 0,
	diskon real default 0,
	total_harga real NOT NULL,
	foto_pengambilan text,
	kode_layanan varchar(10) NOT NULL,
	kode_tarif varchar(10),
	email_staf varchar(50) NOT NULL,
	email_kurir_antar varchar(50),
	email_kurir_jemput varchar(50),
	PRIMARY KEY(email, tanggal),
	FOREIGN KEY(email) REFERENCES PELANGGAN(email)
);

--
-- Dumping data for table TRANSAKSI_LAUNDRY
--

INSERT INTO "transaksi_laundry" VALUES
    ('kbarsham0@epa.gov','2020-05-30 09:04:57',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','lpellman2i@jimdo.com','jharkins1y@wp.com','jharkins1y@wp.com'),
    ('osante1@noaa.gov','2019-05-17 04:22:41',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'bbartosik2j@scribd.com',NULL,NULL),
    ('malessandrelli2@thetimes.co.uk','2019-10-16 19:48:49',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','jtolworth2k@independent.co.uk','acandwell20@noaa.gov','acandwell20@noaa.gov'),
    ('cgravy3@marketwatch.com','2020-02-12 17:50:57',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'pdax2l@reference.com',NULL,NULL),
    ('wyersin4@bizjournals.com','2020-01-24 09:24:02',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','gdubble2m@patch.com','iotridge22@bloglines.com','iotridge22@bloglines.com'),
    ('nclimpson5@hostgator.com','2019-06-21 05:49:50',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','amewitt2n@slideshare.net','tmarchello23@newyorker.com','tmarchello23@newyorker.com'),
    ('dharston6@usgs.gov','2019-11-02 22:06:11',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'dguerriero2o@boston.com',NULL,NULL),
    ('edewey7@feedburner.com','2020-09-23 19:36:10',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','msnailham2p@ihg.com','tdottrell25@marketwatch.com','tdottrell25@marketwatch.com'),
    ('jcarslaw8@hhs.gov','2020-01-17 12:44:25',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'atuff2q@nymag.com',NULL,NULL),
    ('fcarolan9@npr.org','2020-12-04 13:49:35',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','psheara2r@discovery.com','ppedrick27@nsw.gov.au','ppedrick27@nsw.gov.au'),
    ('strencha@sourceforge.net','2020-04-05 13:10:19',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','lpellman2i@jimdo.com','bbrixham28@ycombinator.com','bbrixham28@ycombinator.com'),
    ('dcrewb@aboutads.info','2019-08-18 13:56:56',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'bbartosik2j@scribd.com',NULL,NULL),
    ('mdawdaryc@nytimes.com','2019-11-28 17:37:35',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','jtolworth2k@independent.co.uk','lwitherspoon2a@cbsnews.com','lwitherspoon2a@cbsnews.com'),
    ('tpittemd@state.tx.us','2019-05-22 02:17:31',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'pdax2l@reference.com',NULL,NULL),
    ('efantee@dmoz.org','2019-08-29 22:47:11',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','gdubble2m@patch.com','cgladyer2c@dion.ne.jp','cgladyer2c@dion.ne.jp'),
    ('nkrishtopaittisf@cnbc.com','2020-10-09 16:11:50',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','amewitt2n@slideshare.net','hgelderd2d@hud.gov','hgelderd2d@hud.gov'),
    ('sgyppsg@google.fr','2020-03-20 10:44:06',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'dguerriero2o@boston.com',NULL,NULL),
    ('amaghullh@cbc.ca','2020-03-15 22:26:28',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','msnailham2p@ihg.com','llamprey2f@youtu.be','llamprey2f@youtu.be'),
    ('ssarli@paypal.com','2020-09-23 04:10:46',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'atuff2q@nymag.com',NULL,NULL),
    ('chousamanj@studiopress.com','2020-01-17 10:12:56',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','psheara2r@discovery.com','ngomersall2h@seesaa.net','ngomersall2h@seesaa.net'),
    ('ahatliffek@exblog.jp','2020-05-22 10:16:25',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','lpellman2i@jimdo.com','jharkins1y@wp.com','jharkins1y@wp.com'),
    ('vwallingl@nsw.gov.au','2019-04-30 09:22:50',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'bbartosik2j@scribd.com',NULL,NULL),
    ('mzorzonim@friendfeed.com','2020-09-17 07:10:57',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','jtolworth2k@independent.co.uk','acandwell20@noaa.gov','acandwell20@noaa.gov'),
    ('colssonn@yolasite.com','2020-07-30 17:39:01',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'pdax2l@reference.com',NULL,NULL),
    ('gmcgeechano@aboutads.info','2020-03-30 21:40:16',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','gdubble2m@patch.com','iotridge22@bloglines.com','iotridge22@bloglines.com'),
    ('wwillsonp@yale.edu','2019-04-11 20:27:23',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','amewitt2n@slideshare.net','tmarchello23@newyorker.com','tmarchello23@newyorker.com'),
    ('tpackerq@edublogs.org','2020-09-29 22:46:54',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'dguerriero2o@boston.com',NULL,NULL),
    ('lhuddler@sbwire.com','2020-12-19 11:15:08',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','msnailham2p@ihg.com','tdottrell25@marketwatch.com','tdottrell25@marketwatch.com'),
    ('vkirtlands@indiegogo.com','2020-03-04 05:29:18',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'atuff2q@nymag.com',NULL,NULL),
    ('qclassent@newsvine.com','2019-10-08 02:04:35',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','psheara2r@discovery.com','ppedrick27@nsw.gov.au','ppedrick27@nsw.gov.au'),
    ('pcordovau@ning.com','2019-11-20 22:40:08',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','lpellman2i@jimdo.com','bbrixham28@ycombinator.com','bbrixham28@ycombinator.com'),
    ('wwherrettv@google.com','2020-04-01 17:30:27',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'bbartosik2j@scribd.com',NULL,NULL),
    ('tboydlew@ning.com','2020-10-13 17:16:57',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','jtolworth2k@independent.co.uk','lwitherspoon2a@cbsnews.com','lwitherspoon2a@cbsnews.com'),
    ('wgobyx@uol.com.br','2019-04-11 05:02:13',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'pdax2l@reference.com',NULL,NULL),
    ('dpattissony@moonfruit.com','2019-05-26 08:42:35',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','gdubble2m@patch.com','cgladyer2c@dion.ne.jp','cgladyer2c@dion.ne.jp'),
    ('thutablez@symantec.com','2019-07-25 20:52:50',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','amewitt2n@slideshare.net','hgelderd2d@hud.gov','hgelderd2d@hud.gov'),
    ('pcarneck10@goo.ne.jp','2020-11-30 08:04:48',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'dguerriero2o@boston.com',NULL,NULL),
    ('dtoppas11@booking.com','2019-08-17 23:27:10',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','msnailham2p@ihg.com','llamprey2f@youtu.be','llamprey2f@youtu.be'),
    ('ieloi12@ox.ac.uk','2019-09-20 14:30:04',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'atuff2q@nymag.com',NULL,NULL),
    ('htitford13@xrea.com','2020-09-04 23:45:02',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','psheara2r@discovery.com','ngomersall2h@seesaa.net','ngomersall2h@seesaa.net'),
    ('jallner14@liveinternet.ru','2020-02-20 14:33:24',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','lpellman2i@jimdo.com','jharkins1y@wp.com','jharkins1y@wp.com'),
    ('dpeiro15@si.edu','2020-07-10 18:44:22',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'bbartosik2j@scribd.com',NULL,NULL),
    ('cvost16@paypal.com','2020-08-31 16:57:46',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','jtolworth2k@independent.co.uk','acandwell20@noaa.gov','acandwell20@noaa.gov'),
    ('tleemans17@squarespace.com','2019-10-23 17:59:06',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'pdax2l@reference.com',NULL,NULL),
    ('gtours18@godaddy.com','2019-09-15 10:59:35',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','gdubble2m@patch.com','iotridge22@bloglines.com','iotridge22@bloglines.com'),
    ('gporter19@jiathis.com','2020-12-14 17:55:35',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','amewitt2n@slideshare.net','tmarchello23@newyorker.com','tmarchello23@newyorker.com'),
    ('gclampton1a@instagram.com','2019-04-09 11:27:58',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'dguerriero2o@boston.com',NULL,NULL),
    ('nedwinson1b@about.com','2020-10-31 19:54:59',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','msnailham2p@ihg.com','tdottrell25@marketwatch.com','tdottrell25@marketwatch.com'),
    ('kcharke1c@imgur.com','2019-08-14 19:06:22',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'atuff2q@nymag.com',NULL,NULL),
    ('ewinkworth1d@cnet.com','2020-04-06 12:21:05',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','psheara2r@discovery.com','ppedrick27@nsw.gov.au','ppedrick27@nsw.gov.au'),
    ('ltabb1e@imageshack.us','2019-08-03 10:38:49',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','lpellman2i@jimdo.com','bbrixham28@ycombinator.com','bbrixham28@ycombinator.com'),
    ('hbeebe1f@about.com','2019-08-13 05:16:51',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'bbartosik2j@scribd.com',NULL,NULL),
    ('bfurmage1g@reddit.com','2019-10-08 16:23:26',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','jtolworth2k@independent.co.uk','lwitherspoon2a@cbsnews.com','lwitherspoon2a@cbsnews.com'),
    ('dfellini1h@globo.com','2020-05-05 02:59:32',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'pdax2l@reference.com',NULL,NULL),
    ('jchilvers1i@cafepress.com','2019-12-24 05:04:40',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','gdubble2m@patch.com','cgladyer2c@dion.ne.jp','cgladyer2c@dion.ne.jp'),
    ('mbrahmer1j@ezinearticles.com','2020-07-18 02:26:26',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','amewitt2n@slideshare.net','hgelderd2d@hud.gov','hgelderd2d@hud.gov'),
    ('rhugli1k@google.co.jp','2020-12-13 17:48:54',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'dguerriero2o@boston.com',NULL,NULL),
    ('rillwell1l@ibm.com','2019-05-12 14:38:18',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','msnailham2p@ihg.com','llamprey2f@youtu.be','llamprey2f@youtu.be'),
    ('agamble1m@yelp.com','2019-05-16 06:07:39',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'atuff2q@nymag.com',NULL,NULL),
    ('dbert1n@java.com','2020-07-16 00:26:26',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','psheara2r@discovery.com','ngomersall2h@seesaa.net','ngomersall2h@seesaa.net'),
    ('golijve1o@mashable.com','2020-04-06 12:21:05',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','lpellman2i@jimdo.com','jharkins1y@wp.com','jharkins1y@wp.com'),
    ('tlovell1p@tinypic.com','2019-08-03 10:38:49',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'bbartosik2j@scribd.com',NULL,NULL),
    ('bwithinshaw1q@upenn.edu','2019-08-13 05:16:51',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','jtolworth2k@independent.co.uk','acandwell20@noaa.gov','acandwell20@noaa.gov'),
    ('cmcgloin1r@vistaprint.com','2019-10-08 16:23:26',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'pdax2l@reference.com',NULL,NULL),
    ('bbaitey1s@npr.org','2020-05-05 02:59:32',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','gdubble2m@patch.com','iotridge22@bloglines.com','iotridge22@bloglines.com'),
    ('scotes1t@hud.gov','2019-12-24 05:04:40',1,2,10000,'6000','6000',10,19800,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R','SR','amewitt2n@slideshare.net','tmarchello23@newyorker.com','tmarchello23@newyorker.com'),
    ('iwoodyatt1u@google.com.hk','2020-07-18 02:26:26',2,4,10000,default,default,5,9500,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','R',NULL,'dguerriero2o@boston.com',NULL,NULL),
    ('kkneeshaw1v@ehow.com','2020-12-13 17:48:54',3,6,20000,'9000','9000',20,30400,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E','MR','msnailham2p@ihg.com','tdottrell25@marketwatch.com','tdottrell25@marketwatch.com'),
    ('hviollet1w@independent.co.uk','2019-05-12 14:38:18',4,8,20000,default,default,0,20000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','E',NULL,'atuff2q@nymag.com',NULL,NULL),
    ('clothlorien1x@unc.edu','2019-05-16 06:07:39',5,10,30000,'15000','15000',0,60000,'https://s3-ap-southeast-1.amazonaws.com/apic-asset/services-banner/18_serviceBannerPhoto7863.jpg','SE','LR','psheara2r@discovery.com','ppedrick27@nsw.gov.au','ppedrick27@nsw.gov.au');

-- --------------------------------------------------------

--
-- Table structure for table TESTIMONI
--

CREATE TABLE TESTIMONI(
	email varchar(50) NOT NULL,
	tanggal_transaksi timestamp NOT NULL,
	tanggal_testimoni timestamp NOT NULL,
	status text,
	rating integer NOT NULL,
	PRIMARY KEY(email, tanggal_transaksi, tanggal_testimoni),
	FOREIGN KEY(email,tanggal_transaksi) REFERENCES TRANSAKSI_LAUNDRY(email,tanggal)
);

--
-- Dumping data for table TESTIMONI
--

INSERT INTO "testimoni" VALUES
    ('kbarsham0@epa.gov','2020-05-30 09:04:57','2020-05-30 09:04:57','baik',4),
    ('osante1@noaa.gov','2019-05-17 04:22:41','2019-05-17 04:22:41','sangat memuaskan',4),
    ('malessandrelli2@thetimes.co.uk','2019-10-16 19:48:49','2019-10-16 19:48:49','pasti ke sini lagi',5),
    ('cgravy3@marketwatch.com','2020-02-12 17:50:57','2020-02-12 17:50:57','sedikit kurang cepat',3),
    ('wyersin4@bizjournals.com','2020-01-24 09:24:02','2020-01-24 09:24:02','baik',4),
    ('nclimpson5@hostgator.com','2019-06-21 05:49:50','2019-06-21 05:49:50','sangat memuaskan',4),
    ('dharston6@usgs.gov','2019-11-02 22:06:11','2019-11-02 22:06:11','pasti ke sini lagi',5),
    ('edewey7@feedburner.com','2020-09-23 19:36:10','2020-09-23 19:36:10','sedikit kurang cepat',3),
    ('jcarslaw8@hhs.gov','2020-01-17 12:44:25','2020-01-17 12:44:25','baik',4),
    ('fcarolan9@npr.org','2020-12-04 13:49:35','2020-12-04 13:49:35','sangat memuaskan',4),
    ('strencha@sourceforge.net','2020-04-05 13:10:19','2020-04-05 13:10:19','pasti ke sini lagi',5),
    ('dcrewb@aboutads.info','2019-08-18 13:56:56','2019-08-18 13:56:56','sedikit kurang cepat',3),
    ('mdawdaryc@nytimes.com','2019-11-28 17:37:35','2019-11-28 17:37:35','baik',4),
    ('tpittemd@state.tx.us','2019-05-22 02:17:31','2019-05-22 02:17:31','sangat memuaskan',4),
    ('efantee@dmoz.org','2019-08-29 22:47:11','2019-08-29 22:47:11','pasti ke sini lagi',5),
    ('nkrishtopaittisf@cnbc.com','2020-10-09 16:11:50','2020-10-09 16:11:50','sedikit kurang cepat',3),
    ('sgyppsg@google.fr','2020-03-20 10:44:06','2020-03-20 10:44:06','baik',4),
    ('amaghullh@cbc.ca','2020-03-15 22:26:28','2020-03-15 22:26:28','sangat memuaskan',4),
    ('ssarli@paypal.com','2020-09-23 04:10:46','2020-09-23 04:10:46','pasti ke sini lagi',5),
    ('chousamanj@studiopress.com','2020-01-17 10:12:56','2020-01-17 10:12:56','sedikit kurang cepat',3),
    ('ahatliffek@exblog.jp','2020-05-22 10:16:25','2020-05-22 10:16:25','baik',4),
    ('vwallingl@nsw.gov.au','2019-04-30 09:22:50','2019-04-30 09:22:50','sangat memuaskan',4),
    ('mzorzonim@friendfeed.com','2020-09-17 07:10:57','2020-09-17 07:10:57','pasti ke sini lagi',5),
    ('colssonn@yolasite.com','2020-07-30 17:39:01','2020-07-30 17:39:01','sedikit kurang cepat',3),
    ('gmcgeechano@aboutads.info','2020-03-30 21:40:16','2020-03-30 21:40:16','baik',4),
    ('wwillsonp@yale.edu','2019-04-11 20:27:23','2019-04-11 20:27:23','sangat memuaskan',4),
    ('tpackerq@edublogs.org','2020-09-29 22:46:54','2020-09-29 22:46:54','pasti ke sini lagi',5),
    ('lhuddler@sbwire.com','2020-12-19 11:15:08','2020-12-19 11:15:08','sedikit kurang cepat',3),
    ('vkirtlands@indiegogo.com','2020-03-04 05:29:18','2020-03-04 05:29:18','baik',4),
    ('qclassent@newsvine.com','2019-10-08 02:04:35','2019-10-08 02:04:35','sangat memuaskan',4),
    ('pcordovau@ning.com','2019-11-20 22:40:08','2019-11-20 22:40:08','pasti ke sini lagi',5),
    ('wwherrettv@google.com','2020-04-01 17:30:27','2020-04-01 17:30:27','sedikit kurang cepat',3),
    ('tboydlew@ning.com','2020-10-13 17:16:57','2020-10-13 17:16:57','baik',4),
    ('wgobyx@uol.com.br','2019-04-11 05:02:13','2019-04-11 05:02:13','sangat memuaskan',4),
    ('dpattissony@moonfruit.com','2019-05-26 08:42:35','2019-05-26 08:42:35','pasti ke sini lagi',5),
    ('thutablez@symantec.com','2019-07-25 20:52:50','2019-07-25 20:52:50','sedikit kurang cepat',3),
    ('pcarneck10@goo.ne.jp','2020-11-30 08:04:48','2020-11-30 08:04:48','baik',4),
    ('dtoppas11@booking.com','2019-08-17 23:27:10','2019-08-17 23:27:10','sangat memuaskan',4),
    ('ieloi12@ox.ac.uk','2019-09-20 14:30:04','2019-09-20 14:30:04','pasti ke sini lagi',5),
    ('htitford13@xrea.com','2020-09-04 23:45:02','2020-09-04 23:45:02','sedikit kurang cepat',3),
    ('jallner14@liveinternet.ru','2020-02-20 14:33:24','2020-02-20 14:33:24','baik',4),
    ('dpeiro15@si.edu','2020-07-10 18:44:22','2020-07-10 18:44:22','sangat memuaskan',4),
    ('cvost16@paypal.com','2020-08-31 16:57:46','2020-08-31 16:57:46','pasti ke sini lagi',5),
    ('tleemans17@squarespace.com','2019-10-23 17:59:06','2019-10-23 17:59:06','sedikit kurang cepat',3),
    ('gtours18@godaddy.com','2019-09-15 10:59:35','2019-09-15 10:59:35','baik',4),
    ('gporter19@jiathis.com','2020-12-14 17:55:35','2020-12-14 17:55:35','sangat memuaskan',4),
    ('gclampton1a@instagram.com','2019-04-09 11:27:58','2019-04-09 11:27:58','pasti ke sini lagi',5),
    ('nedwinson1b@about.com','2020-10-31 19:54:59','2020-10-31 19:54:59','sedikit kurang cepat',3),
    ('kcharke1c@imgur.com','2019-08-14 19:06:22','2019-08-14 19:06:22','baik',4),
    ('ewinkworth1d@cnet.com','2020-04-06 12:21:05','2020-04-06 12:21:05','sangat memuaskan',4);

-- --------------------------------------------------------

--
-- Table structure for table STATUS_TRANSAKSI
--

CREATE TABLE STATUS_TRANSAKSI(
	email varchar(50) NOT NULL,
	tanggal_transaksi timestamp NOT NULL,
	kode_status varchar(10) NOT NULL,
	time_stamp timestamp NOT NULL,
	PRIMARY KEY(email, tanggal_transaksi, kode_status),
	FOREIGN KEY(email,tanggal_transaksi) REFERENCES TRANSAKSI_LAUNDRY(email,tanggal),
	FOREIGN KEY(kode_status) REFERENCES STATUS(kode_status)
);

--
-- Dumping data for table STATUS_TRANSAKSI
--

INSERT INTO "status_transaksi" VALUES
    ('kbarsham0@epa.gov','2020-05-30 09:04:57',1,'2020-05-30 09:04:57'),
    ('osante1@noaa.gov','2019-05-17 04:22:41',1,'2019-05-17 04:22:41'),
    ('malessandrelli2@thetimes.co.uk','2019-10-16 19:48:49',1,'2019-10-16 19:48:49'),
    ('cgravy3@marketwatch.com','2020-02-12 17:50:57',1,'2020-02-12 17:50:57'),
    ('wyersin4@bizjournals.com','2020-01-24 09:24:02',1,'2020-01-24 09:24:02'),
    ('nclimpson5@hostgator.com','2019-06-21 05:49:50',1,'2019-06-21 05:49:50'),
    ('dharston6@usgs.gov','2019-11-02 22:06:11',1,'2019-11-02 22:06:11'),
    ('edewey7@feedburner.com','2020-09-23 19:36:10',1,'2020-09-23 19:36:10'),
    ('jcarslaw8@hhs.gov','2020-01-17 12:44:25',1,'2020-01-17 12:44:25'),
    ('fcarolan9@npr.org','2020-12-04 13:49:35',1,'2020-12-04 13:49:35'),
    ('strencha@sourceforge.net','2020-04-05 13:10:19',1,'2020-04-05 13:10:19'),
    ('dcrewb@aboutads.info','2019-08-18 13:56:56',1,'2019-08-18 13:56:56'),
    ('mdawdaryc@nytimes.com','2019-11-28 17:37:35',1,'2019-11-28 17:37:35'),
    ('tpittemd@state.tx.us','2019-05-22 02:17:31',1,'2019-05-22 02:17:31'),
    ('efantee@dmoz.org','2019-08-29 22:47:11',1,'2019-08-29 22:47:11'),
    ('nkrishtopaittisf@cnbc.com','2020-10-09 16:11:50',1,'2020-10-09 16:11:50'),
    ('sgyppsg@google.fr','2020-03-20 10:44:06',1,'2020-03-20 10:44:06'),
    ('amaghullh@cbc.ca','2020-03-15 22:26:28',1,'2020-03-15 22:26:28'),
    ('ssarli@paypal.com','2020-09-23 04:10:46',1,'2020-09-23 04:10:46'),
    ('chousamanj@studiopress.com','2020-01-17 10:12:56',1,'2020-01-17 10:12:56'),
    ('ahatliffek@exblog.jp','2020-05-22 10:16:25',1,'2020-05-22 10:16:25'),
    ('vwallingl@nsw.gov.au','2019-04-30 09:22:50',1,'2019-04-30 09:22:50'),
    ('mzorzonim@friendfeed.com','2020-09-17 07:10:57',1,'2020-09-17 07:10:57'),
    ('colssonn@yolasite.com','2020-07-30 17:39:01',1,'2020-07-30 17:39:01'),
    ('gmcgeechano@aboutads.info','2020-03-30 21:40:16',1,'2020-03-30 21:40:16'),
    ('wwillsonp@yale.edu','2019-04-11 20:27:23',1,'2019-04-11 20:27:23'),
    ('tpackerq@edublogs.org','2020-09-29 22:46:54',1,'2020-09-29 22:46:54'),
    ('lhuddler@sbwire.com','2020-12-19 11:15:08',1,'2020-12-19 11:15:08'),
    ('vkirtlands@indiegogo.com','2020-03-04 05:29:18',1,'2020-03-04 05:29:18'),
    ('qclassent@newsvine.com','2019-10-08 02:04:35',1,'2019-10-08 02:04:35'),
    ('pcordovau@ning.com','2019-11-20 22:40:08',1,'2019-11-20 22:40:08'),
    ('wwherrettv@google.com','2020-04-01 17:30:27',1,'2020-04-01 17:30:27'),
    ('tboydlew@ning.com','2020-10-13 17:16:57',1,'2020-10-13 17:16:57'),
    ('wgobyx@uol.com.br','2019-04-11 05:02:13',1,'2019-04-11 05:02:13'),
    ('dpattissony@moonfruit.com','2019-05-26 08:42:35',1,'2019-05-26 08:42:35'),
    ('thutablez@symantec.com','2019-07-25 20:52:50',1,'2019-07-25 20:52:50'),
    ('pcarneck10@goo.ne.jp','2020-11-30 08:04:48',1,'2020-11-30 08:04:48'),
    ('dtoppas11@booking.com','2019-08-17 23:27:10',1,'2019-08-17 23:27:10'),
    ('ieloi12@ox.ac.uk','2019-09-20 14:30:04',1,'2019-09-20 14:30:04'),
    ('htitford13@xrea.com','2020-09-04 23:45:02',1,'2020-09-04 23:45:02'),
    ('jallner14@liveinternet.ru','2020-02-20 14:33:24',1,'2020-02-20 14:33:24'),
    ('dpeiro15@si.edu','2020-07-10 18:44:22',1,'2020-07-10 18:44:22'),
    ('cvost16@paypal.com','2020-08-31 16:57:46',1,'2020-08-31 16:57:46'),
    ('tleemans17@squarespace.com','2019-10-23 17:59:06',1,'2019-10-23 17:59:06'),
    ('gtours18@godaddy.com','2019-09-15 10:59:35',1,'2019-09-15 10:59:35'),
    ('gporter19@jiathis.com','2020-12-14 17:55:35',1,'2020-12-14 17:55:35'),
    ('gclampton1a@instagram.com','2019-04-09 11:27:58',1,'2019-04-09 11:27:58'),
    ('nedwinson1b@about.com','2020-10-31 19:54:59',1,'2020-10-31 19:54:59'),
    ('kcharke1c@imgur.com','2019-08-14 19:06:22',1,'2019-08-14 19:06:22'),
    ('ewinkworth1d@cnet.com','2020-04-06 12:21:05',1,'2020-04-06 12:21:05'),
    ('kbarsham0@epa.gov','2020-05-30 09:04:57',5,'2020-05-30 09:04:58'),
    ('osante1@noaa.gov','2019-05-17 04:22:41',5,'2019-05-17 04:22:42'),
    ('malessandrelli2@thetimes.co.uk','2019-10-16 19:48:49',5,'2019-10-16 19:48:50'),
    ('cgravy3@marketwatch.com','2020-02-12 17:50:57',5,'2020-02-12 17:50:58'),
    ('wyersin4@bizjournals.com','2020-01-24 09:24:02',5,'2020-01-24 09:24:03'),
    ('nclimpson5@hostgator.com','2019-06-21 05:49:50',5,'2019-06-21 05:49:51'),
    ('dharston6@usgs.gov','2019-11-02 22:06:11',5,'2019-11-02 22:06:12'),
    ('edewey7@feedburner.com','2020-09-23 19:36:10',5,'2020-09-23 19:36:11'),
    ('jcarslaw8@hhs.gov','2020-01-17 12:44:25',5,'2020-01-17 12:44:26'),
    ('fcarolan9@npr.org','2020-12-04 13:49:35',5,'2020-12-04 13:49:36'),
    ('strencha@sourceforge.net','2020-04-05 13:10:19',5,'2020-04-05 13:10:20'),
    ('dcrewb@aboutads.info','2019-08-18 13:56:56',5,'2019-08-18 13:56:57'),
    ('mdawdaryc@nytimes.com','2019-11-28 17:37:35',5,'2019-11-28 17:37:36'),
    ('tpittemd@state.tx.us','2019-05-22 02:17:31',5,'2019-05-22 02:17:32'),
    ('efantee@dmoz.org','2019-08-29 22:47:11',5,'2019-08-29 22:47:12'),
    ('nkrishtopaittisf@cnbc.com','2020-10-09 16:11:50',5,'2020-10-09 16:11:51'),
    ('sgyppsg@google.fr','2020-03-20 10:44:06',5,'2020-03-20 10:44:07'),
    ('amaghullh@cbc.ca','2020-03-15 22:26:28',5,'2020-03-15 22:26:29'),
    ('ssarli@paypal.com','2020-09-23 04:10:46',5,'2020-09-23 04:10:47'),
    ('chousamanj@studiopress.com','2020-01-17 10:12:56',5,'2020-01-17 10:12:57'),
    ('ahatliffek@exblog.jp','2020-05-22 10:16:25',5,'2020-05-22 10:16:26'),
    ('vwallingl@nsw.gov.au','2019-04-30 09:22:50',5,'2019-04-30 09:22:51'),
    ('mzorzonim@friendfeed.com','2020-09-17 07:10:57',5,'2020-09-17 07:10:58'),
    ('colssonn@yolasite.com','2020-07-30 17:39:01',5,'2020-07-30 17:39:02'),
    ('gmcgeechano@aboutads.info','2020-03-30 21:40:16',5,'2020-03-30 21:40:17'),
    ('wwillsonp@yale.edu','2019-04-11 20:27:23',5,'2019-04-11 20:27:24'),
    ('tpackerq@edublogs.org','2020-09-29 22:46:54',5,'2020-09-29 22:46:55'),
    ('lhuddler@sbwire.com','2020-12-19 11:15:08',5,'2020-12-19 11:15:09'),
    ('vkirtlands@indiegogo.com','2020-03-04 05:29:18',5,'2020-03-04 05:29:19'),
    ('qclassent@newsvine.com','2019-10-08 02:04:35',5,'2019-10-08 02:04:36'),
    ('pcordovau@ning.com','2019-11-20 22:40:08',5,'2019-11-20 22:40:09'),
    ('wwherrettv@google.com','2020-04-01 17:30:27',5,'2020-04-01 17:30:28'),
    ('tboydlew@ning.com','2020-10-13 17:16:57',5,'2020-10-13 17:16:58'),
    ('wgobyx@uol.com.br','2019-04-11 05:02:13',5,'2019-04-11 05:02:14'),
    ('dpattissony@moonfruit.com','2019-05-26 08:42:35',5,'2019-05-26 08:42:36'),
    ('thutablez@symantec.com','2019-07-25 20:52:50',5,'2019-07-25 20:52:51'),
    ('pcarneck10@goo.ne.jp','2020-11-30 08:04:48',5,'2020-11-30 08:04:49'),
    ('dtoppas11@booking.com','2019-08-17 23:27:10',5,'2019-08-17 23:27:11'),
    ('ieloi12@ox.ac.uk','2019-09-20 14:30:04',5,'2019-09-20 14:30:05'),
    ('htitford13@xrea.com','2020-09-04 23:45:02',5,'2020-09-04 23:45:03'),
    ('jallner14@liveinternet.ru','2020-02-20 14:33:24',5,'2020-02-20 14:33:25'),
    ('dpeiro15@si.edu','2020-07-10 18:44:22',5,'2020-07-10 18:44:23'),
    ('cvost16@paypal.com','2020-08-31 16:57:46',5,'2020-08-31 16:57:47'),
    ('tleemans17@squarespace.com','2019-10-23 17:59:06',5,'2019-10-23 17:59:07'),
    ('gtours18@godaddy.com','2019-09-15 10:59:35',5,'2019-09-15 10:59:36'),
    ('gporter19@jiathis.com','2020-12-14 17:55:35',5,'2020-12-14 17:55:36'),
    ('gclampton1a@instagram.com','2019-04-09 11:27:58',5,'2019-04-09 11:27:59'),
    ('nedwinson1b@about.com','2020-10-31 19:54:59',5,'2020-11-30 08:04:49'),
    ('kcharke1c@imgur.com','2019-08-14 19:06:22',5,'2019-08-14 19:06:23'),
    ('ewinkworth1d@cnet.com','2020-04-06 12:21:05',5,'2020-04-06 12:21:06');

-- --------------------------------------------------------

--
-- Table structure for table STAF
--

CREATE TABLE STAF(
	email varchar(50) NOT NULL,
	npwp varchar(20) NOT NULL,
	no_rekening varchar(20) NOT NULL,
	nama_bank varchar(50) NOT NULL,
	kantor_cabang varchar(50) NOT NULL,
	PRIMARY KEY(email),
	FOREIGN KEY(email) REFERENCES PENGGUNA(email)
);

--
-- Dumping data for table STAF
--

INSERT INTO "staf" VALUES
    ('lpellman2i@jimdo.com',1000000001,1000000001,'BRI','Depok'),
    ('bbartosik2j@scribd.com',1000000002,1000000002,'BNI','Bandung'),
    ('jtolworth2k@independent.co.uk',1000000003,1000000003,'BCA','Jakarta'),
    ('pdax2l@reference.com',1000000004,1000000004,'BRI','Depok'),
    ('gdubble2m@patch.com',1000000005,1000000005,'BNI','Bandung'),
    ('amewitt2n@slideshare.net',1000000006,1000000006,'BCA','Jakarta'),
    ('dguerriero2o@boston.com',1000000007,1000000007,'BRI','Depok'),
    ('msnailham2p@ihg.com',1000000008,1000000008,'BNI','Bandung'),
    ('atuff2q@nymag.com',1000000009,1000000009,'BCA','Jakarta'),
    ('psheara2r@discovery.com',1000000010,1000000010,'BRI','Depok');

-- --------------------------------------------------------

--
-- Table structure for table KURIR
--

CREATE TABLE KURIR(
	email varchar(50) NOT NULL,
	npwp varchar(20) NOT NULL,
	no_rekening varchar(20) NOT NULL,
	nama_bank varchar(50) NOT NULL,
	kantor_cabang varchar(50) NOT NULL,
	no_sim varchar(20) NOT NULL,
	nomor_kendaraan varchar(20) NOT NULL,
	jenis_kendaraan varchar(20) NOT NULL,
	PRIMARY KEY(email),
	FOREIGN KEY(email) REFERENCES PENGGUNA(email)
);

--
-- Dumping data for table KURIR
--

INSERT INTO "kurir" VALUES
    ('jharkins1y@wp.com',1000000001,1000000001,'BRI','Depok',1000000001,'B1001HK','motor'),
    ('pspaven1z@usatoday.com',1000000002,1000000002,'BNI','Bandung',1000000002,'B1002HK','mobil'),
    ('acandwell20@noaa.gov',1000000003,1000000003,'BCA','Jakarta',1000000003,'B1003HK','pickup'),
    ('apresslie21@pen.io',1000000004,1000000004,'BRI','Depok',1000000004,'B1004HK','motor'),
    ('iotridge22@bloglines.com',1000000005,1000000005,'BNI','Bandung',1000000005,'B1005HK','mobil'),
    ('tmarchello23@newyorker.com',1000000006,1000000006,'BCA','Jakarta',1000000006,'B1006HK','pickup'),
    ('ahuie24@bbc.co.uk',1000000007,1000000007,'BRI','Depok',1000000007,'B1007HK','motor'),
    ('tdottrell25@marketwatch.com',1000000008,1000000008,'BNI','Bandung',1000000008,'B1008HK','mobil'),
    ('fdowry26@who.int',1000000009,1000000009,'BCA','Jakarta',1000000009,'B1009HK','pickup'),
    ('ppedrick27@nsw.gov.au',1000000010,1000000010,'BRI','Depok',1000000010,'B1010HK','motor'),
    ('bbrixham28@ycombinator.com',1000000011,1000000011,'BRI','Depok',1000000011,'B2001HK','mobil'),
    ('kgobel29@technorati.com',1000000012,1000000012,'BNI','Bandung',1000000012,'B2002HK','pickup'),
    ('lwitherspoon2a@cbsnews.com',1000000013,1000000013,'BCA','Jakarta',1000000013,'B2003HK','motor'),
    ('gturbat2b@msu.edu',1000000014,1000000014,'BRI','Depok',1000000014,'B2004HK','mobil'),
    ('cgladyer2c@dion.ne.jp',1000000015,1000000015,'BNI','Bandung',1000000015,'B2005HK','pickup'),
    ('hgelderd2d@hud.gov',1000000016,1000000016,'BCA','Jakarta',1000000016,'B2006HK','motor'),
    ('lcollcott2e@multiply.com',1000000017,1000000017,'BRI','Depok',1000000017,'B2007HK','mobil'),
    ('llamprey2f@youtu.be',1000000018,1000000018,'BNI','Bandung',1000000018,'B2008HK','pickup'),
    ('ctranfield2g@creativecommons.org',1000000019,1000000019,'BCA','Jakarta',1000000019,'B2009HK','motor'),
    ('ngomersall2h@seesaa.net',1000000020,1000000020,'BRI','Depok',1000000020,'B2010HK','mobil');

-- --------------------------------------------------------

--
-- Table structure for table DAFTAR_LAUNDRY
--

CREATE TABLE DAFTAR_LAUNDRY(
	email varchar(50) NOT NULL,
	tanggal_transaksi timestamp NOT NULL,
	no_urut integer NOT NULL,
	jumlah_item integer NOT NULL,
	kode_item varchar(10) NOT NULL,
	PRIMARY KEY(email, tanggal_transaksi, no_urut),
	FOREIGN KEY(email,tanggal_transaksi) REFERENCES TRANSAKSI_LAUNDRY(email,tanggal),
	FOREIGN KEY(kode_item) REFERENCES item(kode_item)
);

--
-- Dumping data for table DAFTAR_LAUNDRY
--

INSERT INTO "daftar_laundry" VALUES
    ('kbarsham0@epa.gov','2020-05-30 09:04:57',1,2,1),
    ('osante1@noaa.gov','2019-05-17 04:22:41',1,4,2),
    ('malessandrelli2@thetimes.co.uk','2019-10-16 19:48:49',1,6,3),
    ('cgravy3@marketwatch.com','2020-02-12 17:50:57',1,8,4),
    ('wyersin4@bizjournals.com','2020-01-24 09:24:02',1,10,5),
    ('nclimpson5@hostgator.com','2019-06-21 05:49:50',1,2,6),
    ('dharston6@usgs.gov','2019-11-02 22:06:11',1,4,7),
    ('edewey7@feedburner.com','2020-09-23 19:36:10',1,6,8),
    ('jcarslaw8@hhs.gov','2020-01-17 12:44:25',1,8,9),
    ('fcarolan9@npr.org','2020-12-04 13:49:35',1,10,10),
    ('strencha@sourceforge.net','2020-04-05 13:10:19',1,2,1),
    ('dcrewb@aboutads.info','2019-08-18 13:56:56',1,4,2),
    ('mdawdaryc@nytimes.com','2019-11-28 17:37:35',1,6,3),
    ('tpittemd@state.tx.us','2019-05-22 02:17:31',1,8,4),
    ('efantee@dmoz.org','2019-08-29 22:47:11',1,10,5),
    ('nkrishtopaittisf@cnbc.com','2020-10-09 16:11:50',1,2,6),
    ('sgyppsg@google.fr','2020-03-20 10:44:06',1,4,7),
    ('amaghullh@cbc.ca','2020-03-15 22:26:28',1,6,8),
    ('ssarli@paypal.com','2020-09-23 04:10:46',1,8,9),
    ('chousamanj@studiopress.com','2020-01-17 10:12:56',1,10,10),
    ('ahatliffek@exblog.jp','2020-05-22 10:16:25',1,2,1),
    ('vwallingl@nsw.gov.au','2019-04-30 09:22:50',1,4,2),
    ('mzorzonim@friendfeed.com','2020-09-17 07:10:57',1,6,3),
    ('colssonn@yolasite.com','2020-07-30 17:39:01',1,8,4),
    ('gmcgeechano@aboutads.info','2020-03-30 21:40:16',1,10,5),
    ('wwillsonp@yale.edu','2019-04-11 20:27:23',1,2,6),
    ('tpackerq@edublogs.org','2020-09-29 22:46:54',1,4,7),
    ('lhuddler@sbwire.com','2020-12-19 11:15:08',1,6,8),
    ('vkirtlands@indiegogo.com','2020-03-04 05:29:18',1,8,9),
    ('qclassent@newsvine.com','2019-10-08 02:04:35',1,10,10),
    ('pcordovau@ning.com','2019-11-20 22:40:08',1,2,1),
    ('wwherrettv@google.com','2020-04-01 17:30:27',1,4,2),
    ('tboydlew@ning.com','2020-10-13 17:16:57',1,6,3),
    ('wgobyx@uol.com.br','2019-04-11 05:02:13',1,8,4),
    ('dpattissony@moonfruit.com','2019-05-26 08:42:35',1,10,5),
    ('thutablez@symantec.com','2019-07-25 20:52:50',1,2,6),
    ('pcarneck10@goo.ne.jp','2020-11-30 08:04:48',1,4,7),
    ('dtoppas11@booking.com','2019-08-17 23:27:10',1,6,8),
    ('ieloi12@ox.ac.uk','2019-09-20 14:30:04',1,8,9),
    ('htitford13@xrea.com','2020-09-04 23:45:02',1,10,10),
    ('jallner14@liveinternet.ru','2020-02-20 14:33:24',1,2,1),
    ('dpeiro15@si.edu','2020-07-10 18:44:22',1,4,2),
    ('cvost16@paypal.com','2020-08-31 16:57:46',1,6,3),
    ('tleemans17@squarespace.com','2019-10-23 17:59:06',1,8,4),
    ('gtours18@godaddy.com','2019-09-15 10:59:35',1,10,5),
    ('gporter19@jiathis.com','2020-12-14 17:55:35',1,2,6),
    ('gclampton1a@instagram.com','2019-04-09 11:27:58',1,4,7),
    ('nedwinson1b@about.com','2020-10-31 19:54:59',1,6,8),
    ('kcharke1c@imgur.com','2019-08-14 19:06:22',1,8,9),
    ('ewinkworth1d@cnet.com','2020-04-06 12:21:05',1,10,10),
    ('ltabb1e@imageshack.us','2019-08-03 10:38:49',1,2,1),
    ('hbeebe1f@about.com','2019-08-13 05:16:51',1,4,2),
    ('bfurmage1g@reddit.com','2019-10-08 16:23:26',1,6,3),
    ('dfellini1h@globo.com','2020-05-05 02:59:32',1,8,4),
    ('jchilvers1i@cafepress.com','2019-12-24 05:04:40',1,10,5),
    ('mbrahmer1j@ezinearticles.com','2020-07-18 02:26:26',1,2,6),
    ('rhugli1k@google.co.jp','2020-12-13 17:48:54',1,4,7),
    ('rillwell1l@ibm.com','2019-05-12 14:38:18',1,6,8),
    ('agamble1m@yelp.com','2019-05-16 06:07:39',1,8,9),
    ('dbert1n@java.com','2020-07-16 00:26:26',1,10,10),
    ('golijve1o@mashable.com','2020-04-06 12:21:05',1,2,1),
    ('tlovell1p@tinypic.com','2019-08-03 10:38:49',1,4,2),
    ('bwithinshaw1q@upenn.edu','2019-08-13 05:16:51',1,6,3),
    ('cmcgloin1r@vistaprint.com','2019-10-08 16:23:26',1,8,4),
    ('bbaitey1s@npr.org','2020-05-05 02:59:32',1,10,5),
    ('scotes1t@hud.gov','2019-12-24 05:04:40',1,2,6),
    ('iwoodyatt1u@google.com.hk','2020-07-18 02:26:26',1,4,7),
    ('kkneeshaw1v@ehow.com','2020-12-13 17:48:54',1,6,8),
    ('hviollet1w@independent.co.uk','2019-05-12 14:38:18',1,8,9),
    ('clothlorien1x@unc.edu','2019-05-16 06:07:39',1,10,10);
	
-- --------------------------------------------------------

--
-- Add column courier_status into KURIR table
--

alter table kurir add courier_status varchar(15) default 'Available'; 
-- --------------------------------------------------------

--
-- Add on delete cascade on transaksi laundry
-- 

ALTER TABLE daftar_laundry
DROP CONSTRAINT "daftar_laundry_email_tanggal_transaksi_fkey",
ADD CONSTRAINT "daftar_laundry_email_tanggal_transaksi_fkey" FOREIGN KEY (email, tanggal_transaksi)
REFERENCES transaksi_laundry(email, tanggal) ON DELETE CASCADE;

ALTER TABLE status_transaksi
DROP CONSTRAINT "status_transaksi_email_tanggal_transaksi_fkey",
ADD CONSTRAINT "status_transaksi_email_tanggal_transaksi_fkey" FOREIGN KEY (email, tanggal_transaksi)
REFERENCES transaksi_laundry(email, tanggal) ON DELETE CASCADE;

ALTER TABLE testimoni
DROP CONSTRAINT "testimoni_email_tanggal_transaksi_fkey",
ADD CONSTRAINT "testimoni_email_tanggal_transaksi_fkey" FOREIGN KEY (email, tanggal_transaksi)
REFERENCES transaksi_laundry(email, tanggal) ON DELETE CASCADE;

-----------------------------------------------------------------------------------