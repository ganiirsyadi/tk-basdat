--
-- Stored Procedure
--
create or replace function check_email() returns trigger as 
$$
declare
    email_exist boolean;
begin
    email_exist := (select email 
                    from pengguna 
                    where email = NEW.email) != null;
    if (email_exist) then
        raise exception 'Email tersebut sudah terdaftar';
    end if;
    return NEW;
end;
$$
language plpgsql;

--
-- Trigger
--
create trigger check_email_trigger before insert or update
on pengguna for each row execute procedure check_email();