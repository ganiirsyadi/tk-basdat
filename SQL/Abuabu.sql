--
-- Stored Procedure
-- Menggunakan asumsi total biaya di transaksi akan dikurangi sebanyak
-- saldo yang dimiliki (jika saldo lebih banyak maka total biaya menjadi 0)
--
create or replace function count_total_price() returns trigger as 
$$
declare
    emailpelanggan varchar(50);
    pakai_antar_jemput boolean;
    biaya_antar_jemput real;
    saldo real;
begin
    emailpelanggan := NEW.email;
    -- SR, MR, LR merupakan kode tarif sesuai seeding pada tarif_antar_jemput
    pakai_antar_jemput := (NEW.kode_tarif in ('SR', 'MR', 'LR'));
    if (pakai_antar_jemput) then
    biaya_antar_jemput := (select harga from tarif_antar_jemput
                            where kode = NEW.kode_tarif);
    else
    biaya_antar_jemput := 0;
    end if;
    NEW.total_harga := ((NEW.biaya_laundry * NEW.berat + biaya_antar_jemput)
                        * ((100 - NEW.diskon) / 100)); 
    saldo := (select saldo_dpay from pelanggan where email = emailpelanggan);
    if (saldo >= NEW.total_harga) then
    update pelanggan set saldo_dpay = (saldo - NEW.total_harga) where email = emailpelanggan;
    NEW.total_harga := 0;
    else
    update pelanggan set saldo_dpay = 0 where email = emailpelanggan;
    NEW.total_harga := (NEW.total_harga - saldo);
    end if;
    return NEW;
end;
$$
language plpgsql;

--
-- Trigger
--
create trigger count_total_price_trigger before insert or update
on transaksi_laundry for each row execute procedure count_total_price();