--
-- Stored Procedure
--
CREATE OR REPLACE FUNCTION update_total_item()
RETURNS trigger as
$$
BEGIN
    UPDATE TRANSAKSI_LAUNDRY
    SET total_item = (total_item + NEW.jumlah_item)
    WHERE email = NEW.email AND tanggal = NEW.tanggal_transaksi;
	RETURN NEW;
END
$$
language plpgsql;


--
-- Trigger
--
CREATE TRIGGER update_total_item_trigger
BEFORE INSERT ON daftar_laundry
FOR EACH ROW
EXECUTE PROCEDURE update_total_item(); 
