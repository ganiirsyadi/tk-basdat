create or replace function fill_credits() returns trigger as $$
begin
    if (tg_op = 'INSERT') then
        update pelanggan set saldo_dpay = saldo_dpay + new.nominal where pelanggan.email = new.email;
        return new;
-- realistically top up should only allow insert queries but it is unclear whether update and delete is restricted so here they are for continuity's sake
    elsif (tg_op = 'UPDATE') then
        update pelanggan set saldo_dpay = saldo_dpay - old.nominal + new.nominal where pelanggan.email = new.email;
        return new;
    elsif (tg_op = 'DELETE') then
        update pelanggan set saldo_dpay = saldo_dpay - old.nominal where pelanggan.email = old.email;
        return old;
    end if;
end;
$$ language plpgsql;

create trigger trigger_fill_credits before insert or update or delete on transaksi_topup_dpay for each row execute procedure fill_credits();