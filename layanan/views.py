import datetime
from django.db import connection
from django.shortcuts import redirect, render

# Create your views here.


def read_layanan(request):
    if request.session["user_role"] in ['staff', 'kurir', 'pelanggan']:
        data = []
        context = {}
        # mengambil semua daftar layanan
        with connection.cursor() as c:
            c.execute("select * from layanan")
            dataSQL = c.fetchall()
            # 0 -> kode layanan, 1 -> nama layanan, 2 -> durasi, 3 -> harga
            for x in dataSQL:
                data.append((str(x[0]), str(x[1]), str(x[2]), x[3]))
            context["data"] = data
        return render(request, 'layanan/daftar_layanan.html', context)


def update_layanan(request):
    if request.session["user_role"] == "staff":
        context = {}
        if request.method == "GET":
            # jika data ini ada di parameter get
            try:
                context["kodeLayanan"] = request.GET["kodeLayanan"]
                context["namaLayanan"] = request.GET["namaLayanan"]
                context["durasi"] = request.GET["durasi"]
                context["harga"] = request.GET["harga"]
            # jika tidak ada
            except:
                return redirect('layanan:read_layanan')
            return render(request, 'layanan/update_layanan.html', context)
        if request.method == 'POST':
            # melakukan update
            with connection.cursor() as c:
                kodeLayanan = request.POST["kodeLayanan"]
                namaLayanan = request.POST["namaLayanan"]
                durasi = request.POST["durasi"]
                harga = request.POST["harga"]
                command = f"update layanan set harga_layanan = {harga}, "
                command += f"nama_layanan = '{namaLayanan}', durasi_layanan = '{durasi}' "
                command += f"where kode_layanan = '{kodeLayanan}'"
                # jika update berhasil
                try:
                    c.execute(command)
                    print("success")
                # jika gagal
                except Exception as e:
                    print(e)
            return redirect('layanan:read_layanan')
