from django.urls import path

from . import views

app_name = "layanan"

urlpatterns = [
    path('', views.read_layanan, name='read_layanan'),
    path('update/', views.update_layanan, name='update_layanan'),
]
