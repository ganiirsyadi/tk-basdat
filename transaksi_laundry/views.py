import datetime
from django.db import connection
from django.shortcuts import redirect, render
from django.contrib import messages

# Create your views here.


def read_transaksi(request):
    if request.session["user_role"] in ["staff", "pelanggan", "kurir"]:
        data = []
        context = {}
        # mengambil semua daftar transaksi_laundry
        with connection.cursor() as c:
            c.execute("select * from transaksi_laundry")
            dataSQL = c.fetchall()
            # convert date object to str (date object ada di index ke-1)
            for x in dataSQL:
                data.append((x[0], str(x[1]), x[2], x[3], x[10], x[11], x[8], x[12], x[13]))
            context["data"] = data
        return render(request, 'transaksi_laundry/daftar_transaksi.html', context)


def update_transaksi(request):
    if request.session["user_role"] == "staff":
        context = {}
        if request.method == "GET":
            # jika data ini ada di parameter get
            try:
                context["email"] = request.GET["email"]
                context["tanggal"] = request.GET["tanggal"]
                context["berat"] = request.GET["berat"]
                context["total_item"] = request.GET["total_item"]
                context["kode_layanan"] = request.GET["kode_layanan"]
                context["kode_tarif"] = request.GET["kode_tarif"]
                context["total_biaya"] = request.GET["total_biaya"]
                context["email_staf"] = request.GET["email_staf"]
                context["email_kurir"] = request.GET["email_kurir"]
            # jika tidak ada
            except:
                return redirect('transaksi_laundry:read_transaksi')
            # mengambil semua kode item
            with connection.cursor() as c:
                c.execute("select * from tarif_antar_jemput")
                context["daftar_tarif"] = c.fetchall()
            with connection.cursor() as c:
                c.execute("select * from layanan")
                context["daftar_layanan"] = c.fetchall()
            with connection.cursor() as c:
                c.execute("select * from staf")
                context["daftar_staf"] = c.fetchall()
            with connection.cursor() as c:
                c.execute("select * from kurir")
                context["daftar_kurir"] = c.fetchall()
            return render(request, 'transaksi_laundry/update_transaksi.html', context)
        if request.method == 'POST':
            # melakukan update
            with connection.cursor() as c:
                email = request.POST["email"]
                tanggal = request.POST["tanggal"]
                berat = request.POST["berat"]
                total_item = request.POST["total_item"]
                kode_layanan = request.POST["kode_layanan"]
                kode_tarif = request.POST["kode_tarif"]
                total_biaya = request.POST["total_biaya"]
                email_staf = request.POST["email_staf"]
                email_kurir = request.POST["email_kurir"]
                command = f"update transaksi_laundry set berat = {berat},"
                command += f"total_item = {total_item}, kode_layanan = '{kode_layanan}', "
                command += f"total_harga = {total_biaya}, kode_tarif = '{kode_tarif}', "
                command += f"email_staf = '{email_staf}', email_kurir_antar = '{email_kurir}', email_kurir_jemput = '{email_kurir}' "
                command += f"where email = '{email}' and tanggal = '{tanggal}'"
                # jika update berhasil
                try:
                    c.execute(command)
                    print("success")
                # jika gagal
                except Exception as e:
                    print(e)
            return redirect('transaksi_laundry:read_transaksi')


def delete_transaksi(request):
    if request.session["user_role"] == "staff":
        if request.method == "POST":
            with connection.cursor() as c:
                email = request.POST["email"]
                tanggal = request.POST["tanggal"]
                command = f"delete from transaksi_laundry where email = '{email}' and "
                command += f"tanggal = '{tanggal}'"
                try:
                    c.execute(command)
                    print("success")
                except Exception as e:
                    print(e)
                return redirect('transaksi_laundry:read_transaksi')


def create_transaksi(request):
    if request.session["user_role"] == "staff":
        context = {}
        if request.method == 'POST':
            try:
                email = request.POST["email"]
                tanggal = str(datetime.datetime.now())
                berat = request.POST["berat"]
                total_item = request.POST["total_item"]
                kode_layanan = request.POST["kode_layanan"]
                total_biaya = request.POST["total_biaya"]
                email_staf = request.POST["email_staf"]
                
                try:
                    kode_tarif = request.POST["kode_tarif"]
                    email_kurir = request.POST["email_kurir"]
                    with connection.cursor() as c:
                        c.execute(f"select harga from tarif_antar_jemput where kode = '{kode_tarif}'")
                        biaya_antar = c.fetchone()[0]
                except:
                    biaya_antar = 0
                    kode_tarif = ""
                    email_kurir = ""
                # select biaya laundry based on layanan
                with connection.cursor() as c:
                    c.execute(f"select harga_layanan from layanan where kode_layanan = '{kode_layanan}'")
                    biaya_laundry = c.fetchone()[0]
                # insert data
                with connection.cursor() as c:
                    command = "INSERT INTO transaksi_laundry VALUES "
                    command += f"('{email}','{tanggal}',{berat},{total_item},{biaya_laundry},{biaya_antar},{biaya_antar},default,{total_biaya},null,'{kode_layanan}','{kode_tarif}','{email_staf}','{email_kurir}','{email_kurir}')"
                    c.execute(command)
            except Exception as e:
                print(e)
                return redirect('transaksi_laundry:create_transaksi')
            return redirect('transaksi_laundry:read_transaksi')
        else:
            # mengambil semua kode
            with connection.cursor() as c:
                c.execute("select email from pelanggan")
                context["daftar_email"] = c.fetchall()
            with connection.cursor() as c:
                c.execute("select * from tarif_antar_jemput")
                context["daftar_tarif"] = c.fetchall()
            with connection.cursor() as c:
                c.execute("select * from layanan")
                context["daftar_layanan"] = c.fetchall()
            with connection.cursor() as c:
                c.execute("select * from staf")
                context["daftar_staf"] = c.fetchall()
            with connection.cursor() as c:
                c.execute("select * from kurir")
                context["daftar_kurir"] = c.fetchall()
            return render(request, 'transaksi_laundry/create_transaksi.html', context)
