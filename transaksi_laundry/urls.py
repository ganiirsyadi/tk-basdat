from django.urls import path
from . import views

app_name = "transaksi_laundry"

urlpatterns = [
    path('', views.read_transaksi, name='read_transaksi'),
    path('update/', views.update_transaksi, name='update_transaksi'),
    path('create/', views.create_transaksi, name='create_transaksi'),
    path('delete/', views.delete_transaksi, name='delete_transaksi'),
]
 