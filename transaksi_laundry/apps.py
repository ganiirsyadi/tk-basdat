from django.apps import AppConfig


class TransaksiLaundryConfig(AppConfig):
    name = 'transaksi_laundry'
